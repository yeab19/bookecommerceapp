import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../../state.dart';
import '../bookDescription/coverImage.dart';
import '../bookDescription/bookAuthor.dart';

import '../readBook/readBook.dart';

List books = [];

class LibraryPage extends StatefulWidget {
  @override
  _LibraryPageState createState() => _LibraryPageState();
}

class _LibraryPageState extends State<LibraryPage>
    with SingleTickerProviderStateMixin {
  @override
  TabController _tabController;

  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
  }

  void didChangeDependencies() async {
    await getLibrary();
  }

  Future<void> getLibrary() async {
    var userid = Provider.of<store>(context, listen: false).getUserId;
    var token = Provider.of<store>(context, listen: false).getToken;

    var response = await getUserLibrary(userid, token);
    books = response['data'];
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    print(height / 3.5);

    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: height / 20),
      child: SingleChildScrollView(
          child: Column(children: [
        Row(
          children: [
            Container(
              padding: EdgeInsets.only(right: 20.0),
              child: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                  size: 38.0,
                ),
              ),
            ),
          ],
        ),
        Text("My Library", style: kLibraryStyle),
        Padding(
          padding: EdgeInsets.only(top: 10.0),
          child: TabBar(
              labelColor: Colors.black,
              unselectedLabelColor: subheaderColor,
              indicatorColor: buttonColor,
              indicatorWeight: 6,
              labelStyle: kWorkStyle,
              unselectedLabelStyle: kWorkStyle,
              tabs: [Text("All"), Text("Reading now"), Text("Completed")],
              controller: _tabController),
        ),
        Container(
            height: 370,
            child: TabBarView(
              children: [
                Container(
                    child: ListView.builder(
                        itemCount: books.length,
                        itemBuilder: (context, index) {
                          return Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  CoverImage(
                                      book: books[index],
                                      height: (height / 3.5).toInt(),
                                      width: (width / 3).toInt()),
                                  Container(
                                    width: width / 2,
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(books[index]['title'],
                                              maxLines: 3, style: kWorkStyle),
                                          BookAuthor(book: books[index]),
                                          Text("403 of 616 pages",
                                              style: kLibraryDetailStyle),
                                          LinearProgressIndicator(
                                              backgroundColor: subheaderColor,
                                              valueColor:
                                                  new AlwaysStoppedAnimation<
                                                      Color>(buttonColor),
                                              value: 0.5),
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(100)),
                                            ),
                                            child: MaterialButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20)),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return BookReader();
                                                  }));
                                                },
                                                elevation: 0,
                                                color: buttonColor,
                                                child:
                                                    Text("Continue Reading")),
                                          )
                                        ]),
                                  )
                                ]),
                          );
                        })),
                Container(
                    child: ListView.builder(
                        itemCount: books.length,
                        itemBuilder: (context, index) {
                          return Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  CoverImage(
                                      book: books[index],
                                      height: (height / 3.5).toInt(),
                                      width: (width / 3).toInt()),
                                  Container(
                                    width: width / 2,
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(books[index]['title'],
                                              maxLines: 3, style: kWorkStyle),
                                          Text("Michele Obama",
                                              style: kLibraryDetailStyle),
                                          Text("403 of 616 pages",
                                              style: kLibraryDetailStyle),
                                          LinearProgressIndicator(
                                              backgroundColor: subheaderColor,
                                              valueColor:
                                                  new AlwaysStoppedAnimation<
                                                      Color>(buttonColor),
                                              value: 0.5),
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(100)),
                                            ),
                                            child: MaterialButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20)),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return BookReader();
                                                  }));
                                                },
                                                elevation: 0,
                                                color: buttonColor,
                                                child:
                                                    Text("Continue Reading")),
                                          )
                                        ]),
                                  )
                                ]),
                          );
                        })),
                Container(
                    child: ListView.builder(
                        itemCount: books.length,
                        itemBuilder: (context, index) {
                          return Container(
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  CoverImage(
                                      book: books[index],
                                      height: (height / 3.5).toInt(),
                                      width: (width / 3).toInt()),
                                  Container(
                                    width: width / 2,
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceAround,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(books[index]['title'],
                                              maxLines: 3, style: kWorkStyle),
                                          Text("Michele Obama",
                                              style: kLibraryDetailStyle),
                                          Text("403 of 616 pages",
                                              style: kLibraryDetailStyle),
                                          LinearProgressIndicator(
                                              backgroundColor: subheaderColor,
                                              valueColor:
                                                  new AlwaysStoppedAnimation<
                                                      Color>(buttonColor),
                                              value: 0.5),
                                          Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(100)),
                                            ),
                                            child: MaterialButton(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(20)),
                                                ),
                                                onPressed: () {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                          builder: (context) {
                                                    return BookReader();
                                                  }));
                                                },
                                                elevation: 0,
                                                color: buttonColor,
                                                child:
                                                    Text("Continue Reading")),
                                          )
                                        ]),
                                  )
                                ]),
                          );
                        }))
              ],
              controller: _tabController,
            ))
      ])),
    ));
  }
}
