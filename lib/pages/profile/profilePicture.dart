import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class ProfilePicture extends StatefulWidget {
  ProfilePicture({
    Key key,
    @required this.user,
    @required this.height,
    @required this.width,
    this.isCircular,
    this.radius,
  });
  final user;
  final int height;
  final int width;
  final double radius;
  bool isCircular = false;

  @override
  _ProfilePictureState createState() => _ProfilePictureState();
}

class _ProfilePictureState extends State<ProfilePicture> {
  @override
  var storage = firebase_storage.FirebaseStorage.instance;
  String ProfileUrl = '';

  void didChangeDependencies() async {
    await loadImage();
  }

  Future<void> loadImage() async {
    var user = widget.user;

    if (user['image'] != null) {
      String url = await storage.ref().child(user['image']).getDownloadURL();
      setState(() {
        ProfileUrl = url;
      });

      print(ProfileUrl);
    }
  }

  Widget build(BuildContext context) {
    return widget.isCircular == null
        ? ProfileUrl != ''
            ? Image(
                image: NetworkImage(ProfileUrl),
                height: widget.height.roundToDouble(),
                width: widget.width.roundToDouble(),
                fit: BoxFit.fill)
            : Image(
                image: AssetImage('images/bookCover.jpg'),
                height: widget.height.roundToDouble(),
                width: widget.width.roundToDouble(),
                fit: BoxFit.fill)
        : CircleAvatar(
            radius: widget.radius == null ? 56.0 : widget.radius,
            backgroundImage: ProfileUrl != ''
                ? NetworkImage(ProfileUrl)
                : AssetImage('images/bookCover.jpg'),
          );
  }
}
