import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import '../profile/profile.dart';
import './profilePicture.dart';
import '../../utils/constants.dart';

class FollowingPage extends StatefulWidget {
  @override
  _FollowingPageState createState() => _FollowingPageState();
}

class _FollowingPageState extends State<FollowingPage> {
  @override
  Widget build(BuildContext context) {
    final List following = ModalRoute.of(context).settings.arguments;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(left: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: height / 25),
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 38.0,
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                "Following",
                style: kBookTitleHeaderStyle,
              )),
          Divider(color: Colors.black),
          ListView.builder(
            shrinkWrap: true,
            itemCount: following.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) {
                            return ProfilePage();
                          },
                          settings:
                              RouteSettings(arguments: following[index])));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ProfilePicture(
                      user: following[index],
                      height: 10,
                      width: 10,
                      isCircular: true,
                    ),
                    Column(
                      children: [
                        following[index]['firstname'] != null &&
                                following[index]['lastname'] != null
                            ? Text(
                                following[index]['firstname'] +
                                    " " +
                                    following[index]['lastname'],
                                style: kAuthornameStyle)
                            : Offstage(),
                        following[index]['firstname'] != null &&
                                following[index]['lastname'] != null
                            ? Text(
                                "@" +
                                    following[index]['firstname'] +
                                    following[index]['lastname'],
                                style: kLibraryDetailStyle)
                            : Offstage()
                      ],
                    ),
                    MaterialButton(
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: buttonColor)),
                        child: Text('Following'))
                  ],
                ),
              );
            },
          )
        ],
      ),
    ));
  }
}
