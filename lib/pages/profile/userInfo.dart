import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import '../../utils/requests.dart';

import './followingPage.dart';
import './followersPage.dart';

import '../../utils/constants.dart';
import '../../state.dart';

class UserInfo extends StatefulWidget {
  UserInfo({Key key, @required this.user, this.isBookDesc});

  final user;
  final isBookDesc;
  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  @override
  List books = [];
  List Following = [];
  List Followers = [];
  bool _isLoading = true;

  void didChangeDependencies() async {
    await getWorks();
    await getfollowers();
    await getfollowing();
  }

  Future<void> getfollowers() async {
    var user = widget.user;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getFollowers(user['id'], token);
    setState(() {
      Followers = response['data'];
    });
  }

  Future<void> getfollowing() async {
    var user = widget.user;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getFollowing(user['id'], token);
    setState(() {
      Following = response['data'];
      print(Following);
      _isLoading = false;
    });
  }

  Future<void> getWorks() async {
    var user = widget.user;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getAuthorBooks(user['id'], token);

    setState(() {
      books = response['data'];
      print(books);
    });
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    print(width / 51.4);
    return !_isLoading
        ? Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            Container(
                width: width / 5.75,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: width / 51.4),
                        child: Text(books.length.toString(),
                            style:
                                widget.isBookDesc != null && widget.isBookDesc
                                    ? kuserinfoblackStyle
                                    : KSearchFieldStyle),
                      ),
                      Text("Works",
                          style: widget.isBookDesc != null && widget.isBookDesc
                              ? kprofileusernameStyle2
                              : kprofileusernameStyle)
                    ])),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) {
                          return FollowersPage();
                        },
                        settings: RouteSettings(arguments: Followers)));
              },
              child: Container(
                  width: width / 4.5,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width / 51.4),
                          child: Text(Followers.length.toString(),
                              style:
                                  widget.isBookDesc != null && widget.isBookDesc
                                      ? kuserinfoblackStyle
                                      : KSearchFieldStyle),
                        ),
                        Text("Followers",
                            style:
                                widget.isBookDesc != null && widget.isBookDesc
                                    ? kprofileusernameStyle2
                                    : kprofileusernameStyle)
                      ])),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) {
                          return FollowingPage();
                        },
                        settings: RouteSettings(arguments: Following)));
              },
              child: Container(
                  width: width / 4.5,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: width / 51.4),
                          child: Text(Following.length.toString(),
                              style:
                                  widget.isBookDesc != null && widget.isBookDesc
                                      ? kuserinfoblackStyle
                                      : KSearchFieldStyle),
                        ),
                        Text("Following",
                            style:
                                widget.isBookDesc != null && widget.isBookDesc
                                    ? kprofileusernameStyle2
                                    : kprofileusernameStyle)
                      ])),
            )
          ])
        : Offstage();
  }
}
