import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import '../../utils/constants.dart';
import '../../state.dart';
import '../profile/profilePicture.dart';
import '../../utils/requests.dart';
import '../bookDescription/coverImage.dart';
import '../bookDescription/bookDescription.dart';
import '../bookDescription/bookAuthor.dart';
import '../bookDescription/bookDescription.dart';

class Works extends StatefulWidget {
  Works({Key key, @required this.user});
  final user;
  @override
  _WorksState createState() => _WorksState();
}

class _WorksState extends State<Works> {
  @override
  List books = [];
  bool _isLoading = true;

  void didChangeDependencies() async {
    await getWorks();
  }

  Future<void> getWorks() async {
    var user = widget.user;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getAuthorBooks(user['id'], token);
    // print(response);
    setState(() {
      books = response['data'];
      print(response);
      _isLoading = false;
    });
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    print(width / 3.21);
    // height: (height ~/ 2.8).toInt(),
    // width: (width / 2.555).toInt(),
    return !_isLoading
        ? ListView.builder(
            itemCount: books.length,
            itemBuilder: (context, index) {
              return Container(
                margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BookDescription(),
                            settings: RouteSettings(arguments: books[index])));
                  },
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      CoverImage(
                        book: books[index],
                        height: (height / 3.5).toInt(),
                        width: (width / 3.21).toInt(),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Container(
                                width: MediaQuery.of(context).size.width / 2,
                                child: Text(books[index]['title'],
                                    maxLines: 3, style: kWorkStyle),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: BookAuthor(book: books[index]),
                            ),
                            Padding(
                              padding: EdgeInsets.only(bottom: 10.0),
                              child: Row(
                                  // mainAxisAlignment: MainAxisAlignment.,
                                  children: [
                                    // Padding(
                                    //   padding: EdgeInsets.only(left: width / 30),
                                    //   child: Icon(Icons.remove_red_eye_outlined),
                                    // ),
                                    // Padding(
                                    //   padding: EdgeInsets.only(left: width / 30),
                                    //   child: Text("270", style: kaboutStyle),
                                    // ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: width / 30),
                                      child: Icon(Icons.comment_rounded),
                                    ),
                                    books[index]['Comments'] != null
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                left: width / 30),
                                            child: Text(
                                                books[index]['Comments']
                                                    .length
                                                    .toString(),
                                                style: kaboutStyle),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                left: width / 30),
                                            child:
                                                Text("0", style: kaboutStyle),
                                          ),
                                    Padding(
                                      padding:
                                          EdgeInsets.only(left: width / 30),
                                      child: Icon(Icons.thumb_up_sharp,
                                          color: Colors.black12),
                                    ),
                                    books[index]['LikedBy'] != null
                                        ? Padding(
                                            padding: EdgeInsets.only(
                                                left: width / 30),
                                            child: Text(
                                                books[index]['LikedBy']
                                                    .length
                                                    .toString(),
                                                style: kaboutStyle),
                                          )
                                        : Padding(
                                            padding: EdgeInsets.only(
                                                left: width / 30),
                                            child:
                                                Text("0", style: kaboutStyle),
                                          )
                                  ]),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            })
        : Container(child: Text("Loading"));
  }
}
