import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import './profilePicture.dart';
import '../../utils/constants.dart';
import '../allAuthors/checkFollow.dart';

class FollowersPage extends StatefulWidget {
  @override
  _FollowersPageState createState() => _FollowersPageState();
}

class _FollowersPageState extends State<FollowersPage> {
  @override
  Widget build(BuildContext context) {
    final List followers = ModalRoute.of(context).settings.arguments;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(left: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top: height / 25),
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 38.0,
              ),
            ),
          ),
          Container(
              padding: EdgeInsets.only(left: 10.0),
              child: Text(
                "Followers",
                style: kBookTitleHeaderStyle,
              )),
          Divider(color: Colors.black),
          ListView.builder(
            shrinkWrap: true,
            itemCount: followers.length,
            itemBuilder: (context, index) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ProfilePicture(
                    user: followers[index],
                    height: 10,
                    width: 10,
                    isCircular: true,
                  ),
                  Column(
                    children: [
                      followers[index]['firstname'] != null &&
                              followers[index]['lastname'] != null
                          ? Text(
                              followers[index]['firstname'] +
                                  " " +
                                  followers[index]['lastname'],
                              style: kAuthornameStyle)
                          : Offstage(),
                      followers[index]['firstname'] != null &&
                              followers[index]['lastname'] != null
                          ? Text(
                              "@" +
                                  followers[index]['firstname'] +
                                  followers[index]['lastname'],
                              style: kLibraryDetailStyle)
                          : Offstage()
                    ],
                  ),
                  CheckFollow(authorId: followers[index]['id'])
                  // MaterialButton(
                  //     shape: RoundedRectangleBorder(
                  //         side: BorderSide(color: buttonColor)),
                  //     child: Text('Following'))
                ],
              );
            },
          )
        ],
      ),
    ));
  }
}
