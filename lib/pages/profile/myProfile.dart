import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import '../../utils/constants.dart';
import '../../state.dart';
import '../../utils/requests.dart';
import '../profile/profilePicture.dart';
import './works.dart';
import './userInfo.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile>
    with SingleTickerProviderStateMixin {
  @override
  TabController _Controller;
  bool isLoading = true;
  Map user = {};

  void initState() {
    _Controller = new TabController(length: 2, vsync: this);
    super.initState();
  }

  void didChangeDependencies() async {
    await getuserdata();
    setState(() {
      isLoading = false;
    });
  }

  getuserdata() async {
    String userid = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getUserDetails(userid, token);
    user = response['data'];
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: !isLoading
            ? Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: height / 70),
                    decoration: BoxDecoration(color: profileColor),
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                icon: Icon(
                                  Icons.arrow_back,
                                  color: buttonColor,
                                  size: 30.0,
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                }),
                            Row(
                              children: [
                                IconButton(
                                    onPressed: () {},
                                    icon: Icon(CupertinoIcons.square_pencil)),
                                IconButton(
                                    onPressed: () {},
                                    icon: Icon(Icons.settings))
                              ],
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                ProfilePicture(
                                    user: user,
                                    // width: 72,
                                    // height: 64,
                                    isCircular: true),
                                // CircleAvatar(
                                //   radius: 52.0,
                                //   backgroundImage: AssetImage(
                                //     'images/micheleObama.png',
                                //   ),
                                // ),
                                Container(
                                  padding:
                                      EdgeInsets.only(top: 2 * (height / 68.3)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          user['firstname'][0].toUpperCase() +
                                              user['firstname'].substring(1) +
                                              " " +
                                              user['lastname'][0]
                                                  .toUpperCase() +
                                              user['lastname'].substring(1),
                                          style: kprofiletitleStyle),
                                      Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                            '@' +
                                                user['firstname'][0]
                                                    .toUpperCase() +
                                                user['firstname'].substring(1) +
                                                " " +
                                                user['lastname'][0]
                                                    .toUpperCase() +
                                                user['lastname'].substring(1),
                                            style: kprofileusernameStyle),
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(
                                              left: width / 40.5,
                                              top: height / 68.3),
                                          child: UserInfo(user: user)),
                                    ],
                                  ),
                                )
                              ]),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 1.8 * (height / 68.3)),
                    child: TabBar(
                        labelColor: Colors.black,
                        unselectedLabelColor: subheaderColor,
                        labelStyle: ktabBarStyle,
                        unselectedLabelStyle: ktabBarStyle,
                        tabs: [
                          Text("About"),
                          Text("Works"),
                          // !myProfile?Text("Following"):Container(width:0,height:0)
                        ],
                        controller: _Controller),
                  ),
                  Container(
                      height: 350,
                      padding: EdgeInsets.only(top: 10.0),
                      child: TabBarView(controller: _Controller, children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                                " Consectetur adipiscing sem auctor venenatis amet et. Eu amet, "
                                "mattis enim nulla leo arcu placerat nam. Sed sed aliquet vitae "
                                "consectetur quis amet ut pharetra. ",
                                style: kaboutStyle),
                            Padding(
                              padding: EdgeInsets.all(30.0),
                              child: Row(
                                children: [
                                  Text("Joined ", style: kJoinDateStyle),
                                  Text(
                                      user['timestamp']
                                          .toString()
                                          .substring(0, 10),
                                      style: kJoinDateStyle),
                                ],
                              ),
                            )
                          ],
                        ),
                        Works(user: user),
                        // !myProfile?Text("Following"):Container(width:0,height:0)
                      ]))
                ],
              )
            : Container(
                height: height,
                child: Center(
                  child: Loading(
                      indicator: BallPulseIndicator(),
                      size: 100,
                      color: Colors.pink),
                ),
              ),
      ),
    );
  }
}
