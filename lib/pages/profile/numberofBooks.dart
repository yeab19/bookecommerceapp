import 'package:bookecommerceapp/state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/requests.dart';
import '../../utils/constants.dart';

class NumberofBooks extends StatefulWidget {
  NumberofBooks({Key key, @required this.user});

  final user;
  @override
  _NumberofBooksState createState() => _NumberofBooksState();
}

class _NumberofBooksState extends State<NumberofBooks> {
  int _noOfBooks;
  bool isLoading = true;

  @override
  void didChangeDependencies() async {
    await getWorks();
    setState(() {
      isLoading = false;
    });
  }

  Future<void> getWorks() async {
    final Map Author = widget.user;
    String token = Provider.of<store>(context, listen: false).getToken;
    var response = await getAuthorBooks(Author['id'], token);

    setState(() {
      _noOfBooks = response['data'].length;
    });
  }

  Widget build(BuildContext context) {
    return Container(
      child: !isLoading
          ? Text(
              "$_noOfBooks books",
              style: kNoofBooksStyle,
            )
          : Offstage(),
    );
  }
}
