import 'package:bookecommerceapp/pages/allAuthors/checkFollow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';

import 'package:loading/loading.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../../state.dart';
import '../profile/profilePicture.dart';
import '../profile/numberofBooks.dart';
import '../profile/profile.dart';

List Authors = [];

class AllAuthors extends StatefulWidget {
  @override
  _AllAuthorsState createState() => _AllAuthorsState();
}

class _AllAuthorsState extends State<AllAuthors> {
  @override
  bool _isLoading = true;
  void initState() {
    getallAuthors();
  }

  getallAuthors() async {
    setState(() {
      _isLoading = true;
    });
    var token = Provider.of<store>(context, listen: false).getToken;
    var response = await getAllAuthors(token);
    print(response);
    if (response['status'] == "success") {
      Authors = response['data'];
      setState(() {
        _isLoading = false;
      });
    }
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        body: Container(
            padding: EdgeInsets.only(top: 30.0),
            child: Column(
              children: [
                Row(children: [
                  Container(
                    padding: EdgeInsets.only(left: 12.0, right: 20.0),
                    child: IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: buttonColor,
                          size: 40.0,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        }),
                  ),
                  Text(
                    "Authors to follow",
                    style: ktitleTextStyle,
                  )
                ]),
                Divider(
                  color: Colors.black,
                ),
                !_isLoading
                    ? Container(
                        child: Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemCount: Authors.length,
                          itemBuilder: (context, index) {
                            var firstname =
                                Authors[index]['firstname'][0].toUpperCase() +
                                    Authors[index]['firstname'].substring(1);
                            var lastname =
                                Authors[index]['lastname'][0].toUpperCase() +
                                    Authors[index]['lastname'].substring(1);
                            String fullname = firstname + " " + lastname;

                            return Column(children: [
                              InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) {
                                              return ProfilePage();
                                            },
                                            settings: RouteSettings(
                                                arguments: Authors[index])));
                                  },
                                  child: Container(
                                      padding: EdgeInsets.only(bottom: 20.0),
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          children: [
                                            ProfilePicture(
                                              user: Authors[index],
                                              width: 72,
                                              height: 64,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              textBaseline:
                                                  TextBaseline.alphabetic,
                                              children: [
                                                Container(
                                                  // width: width / 2.057,
                                                  child: Text(fullname,
                                                      maxLines: 2,
                                                      style:
                                                          kBookTitleHeaderStyle),
                                                ),
                                                Container(
                                                  // width: width / 2.057,
                                                  child: Row(
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 4.0),
                                                        child: Icon(
                                                            CupertinoIcons
                                                                .book_solid,
                                                            color:
                                                                subheaderColor),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 8.0),
                                                        child: NumberofBooks(
                                                            user:
                                                                Authors[index]),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              height: 34,
                                              child: CheckFollow(
                                                authorId: Authors[index]['id'],
                                              ),
                                            )
                                          ])))
                            ]);
                          },
                        ),
                      ))
                    : Padding(
                        padding: EdgeInsets.only(top: height / 2.5),
                        child: Loading(
                          indicator: BallPulseIndicator(),
                          size: 100.0,
                          color: Colors.pink,
                        ),
                      ),
              ],
            )));
  }
}
