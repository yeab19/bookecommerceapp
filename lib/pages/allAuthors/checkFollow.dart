import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class CheckFollow extends StatefulWidget {
  CheckFollow({Key key, @required this.authorId});

  final authorId;
  @override
  _CheckFollowState createState() => _CheckFollowState();
}

class _CheckFollowState extends State<CheckFollow> {
  @override
  List Following = [];
  bool isFollowing = false;
  // bool myAccount = false;

  void didChangeDependencies() async {
    await getfollowing();
    await check();
  }

  Future<void> getfollowing() async {
    String userid = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getFollowing(userid, token);
    setState(() {
      Following = response['data'];
      print(Following);
      // _isLoading = false;
    });
  }

  Future<void> check() {
    for (int i = 0; i < Following.length; i++) {
      if (Following[i]['id'] == widget.authorId) {
        setState(() {
          isFollowing = true;
        });
      }
    }
  }

  FollowAuthor(String AuthorId) async {
    setState(() {
      isFollowing = true;
    });
    var token = Provider.of<store>(context, listen: false).getToken;
    var userId = Provider.of<store>(context, listen: false).getUserId;

    var response = await follow(userId, AuthorId, token);
  }

  UnFollowAuthor(String AuthorId) async {
    setState(() {
      isFollowing = false;
    });
    var token = Provider.of<store>(context, listen: false).getToken;
    var userId = Provider.of<store>(context, listen: false).getUserId;

    var response = await unfollow(userId, AuthorId, token);
  }

  // Future<void> checkFollowing() async {}

  Widget build(BuildContext context) {
    String userid = Provider.of<store>(context, listen: false).getUserId;

    print(widget.authorId);
    return userid != widget.authorId
        ? Container(
            child: Material(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                child: !isFollowing
                    ? MaterialButton(
                        color: buttonColor,
                        onPressed: () {
                          FollowAuthor(widget.authorId);
                        },
                        child: Text("Follow", style: kFollowButtonStyle))
                    : MaterialButton(
                        color: buttonColor,
                        onPressed: () {
                          UnFollowAuthor(widget.authorId);
                        },
                        child: Text("Following", style: kFollowButtonStyle),
                      )),
          )
        : Offstage();
  }
}
