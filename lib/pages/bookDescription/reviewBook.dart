import 'package:bookecommerceapp/pages/bookDescription/bookDescription.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../../state.dart';

final Comment = TextEditingController();
class ReviewBook extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    print(height/20);

    comment() async{
      final Map book = ModalRoute.of(context).settings.arguments;
      String token =Provider.of<store>(context,listen:false).getToken;
      String userid =Provider.of<store>(context,listen:false).getUserId;

      if(!Comment.text.isEmpty){
        print(userid);
        Map Data = {};
        Data['userId'] = userid;
        Data['commentText'] = Comment.text;
        Data['bookId'] = book['id'];

        var response = await commentBook(Data,token);
        Navigator.pop(context);
        // Navigator.push(context,MaterialPageRoute(
        //   builder:(context){
        //    return BookDescription();
        //   },
        //     settings: RouteSettings(
        //         arguments: response['Comments']
        //     )
        // ));
        print(response);
      }
    }


    return Scaffold(
        body: SingleChildScrollView(
          scrollDirection:Axis.vertical,
          child: Container(
            padding: EdgeInsets.only(top: height/20,left:12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(right: 10.0),
                      child: IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        icon: Icon(
                          Icons.arrow_back,
                          color: buttonColor,
                          size: 38.0,
                        ),
                      ),
                    ),
                    Text(
                      "Review",
                      style: kBookTitleHeaderStyle,
                    ),
                  ],
                ),
                Padding(
                  padding:EdgeInsets.only(top:height/18,left:12.0),
                  child:Text(
                      "Let us know how you feel about the book",
                      style: ktextStyle,
                    )
                ),
                Padding(
                  padding:EdgeInsets.only(top:height/36,left:12.0,right:width/8),
                  child:Container(
                    // color:Colors.white,
                    // alignment: Alignment.topLeft,
                    child: TextField(

                      textAlign: TextAlign.start,

                      controller:Comment,
                      maxLines: 7,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(top:6,left:10),
                          hintText: "Comment",
                          hintStyle: ktextStyle,
                          isDense: true,
                          filled:true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(2.0),
                            // borderSide: Border.all(color: Colors.white)
                          )
                      ),
                    ),
                  )
                ),
                Padding(
                  padding: EdgeInsets.only(top:height/18),
                  child: Center(

                    child: Material(
                        color: buttonColor,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        child:MaterialButton(
                          onPressed:(){
                            comment();
                          },

                        child:Container(
                          padding:EdgeInsets.symmetric(horizontal: 15.0),
                          child: Text(
                              "Submit",
                              style:kFollowButtonStyle
                          ),
                        )
                      )
                    ),
                  ),
                )
              ],
            ),
    ),
        ));
  }
}
