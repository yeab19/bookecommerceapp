import 'package:bookecommerceapp/state.dart';
import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';

class PreviewPage extends StatefulWidget {
  PreviewPage({Key key, @required this.book});

  final book;

  @override
  _PreviewPageState createState() => _PreviewPageState();
}

class _PreviewPageState extends State<PreviewPage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
