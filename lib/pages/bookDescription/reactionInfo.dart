import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../profile/profilePicture.dart';
import '../../utils/constants.dart';
import '../allAuthors/checkFollow.dart';

class ReactionInfo extends StatefulWidget {
  ReactionInfo({Key key, @required this.userid, @required this.type});

  final userid;
  final type;

  @override
  _ReactionInfoState createState() => _ReactionInfoState();
}

class _ReactionInfoState extends State<ReactionInfo> {
  @override
  Map user = {};
  bool isLoading = true;

  void didChangeDependencies() async {
    await getUser();
    setState(() {
      isLoading = false;
    });
  }

  Future<void> getUser() async {
    final String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getUserDetails(widget.userid, token);
    if (response['status'] == 'success') {
      setState(() {
        user = response['data'];
      });
    }
  }

  void dispose() {
    user = {};
    super.dispose();
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    String reaction = widget.type;

    return Container(
      child: !isLoading
          ? Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                  child: Row(
                    children: [
                      Stack(children: [
                        Container(
                          width: 70,
                          height: 70,
                          padding: EdgeInsets.zero,
                          child: ProfilePicture(
                            user: user,
                            height: 0,
                            width: 0,
                            isCircular: true,
                            radius: 30,
                          ),
                        ),
                        Positioned(
                            left: 45,
                            top: 45,
                            child: reaction == 'like'
                                ? CircleAvatar(
                                    backgroundImage:
                                        AssetImage('images/Grouplike.png'),
                                    radius: 12,
                                  )
                                : reaction == 'love'
                                    ? CircleAvatar(
                                        backgroundImage:
                                            AssetImage('images/Framelove.png'),
                                        radius: 12,
                                      )
                                    : reaction == 'great'
                                        ? CircleAvatar(
                                            backgroundImage: AssetImage(
                                                'images/Framegreat.png'),
                                            radius: 12,
                                          )
                                        : reaction == 'wow'
                                            ? CircleAvatar(
                                                backgroundImage: AssetImage(
                                                    'images/Groupwow.png'),
                                                radius: 12,
                                              )
                                            : Offstage()),
                      ]),
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Text(user['firstname'] + " " + user['lastname']),
                      ),
                    ],
                  ),
                ),
                Divider(
                  color: Colors.black,
                )
              ],
            )
          : Offstage(),
    );
  }
}
