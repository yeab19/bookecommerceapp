import 'package:bookecommerceapp/state.dart';
import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';

class BookAuthor extends StatefulWidget {
  @override
  BookAuthor({Key key, @required this.book});
  final book;

  @override
  _BookAuthorState createState() => _BookAuthorState();
}

class _BookAuthorState extends State<BookAuthor> {
  var _author = {};
  var fullname = '';

  void initState() {}
  void didChangeDependencies() async {
    await getBookAuthor();
  }

  Future<void> getBookAuthor() async {
    Map book = widget.book;
    var token = Provider.of<store>(context, listen: false).getToken;

    if (book['createdBy'] != null) {
      var response = await getUserDetails(book['createdBy'], token);
      if (response['status'] == "success") {
        setState(() {
          _author = response['data'];

          if (_author.isNotEmpty) {
            fullname = _author['firstname'] + " " + _author['lastname'];
          }
        });
      }
      // print(response);
    }
  }

  Widget build(BuildContext context) {
    print(_author);

    return fullname.isNotEmpty
        ? Text(' $fullname', style: kNoofBooksStyle)
        : Offstage();
  }
}
