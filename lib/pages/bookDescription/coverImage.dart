import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class CoverImage extends StatefulWidget {
  CoverImage(
      {Key key,
      @required this.book,
      @required this.height,
      @required this.width});
  final book;
  final int height;
  final int width;

  @override
  _CoverImageState createState() => _CoverImageState();
}

class _CoverImageState extends State<CoverImage> {
  @override
  var storage = firebase_storage.FirebaseStorage.instance;
  String CoverUrl = '';
  void didChangeDependencies() async {
    await loadImage();
  }

  Future<void> loadImage() async {
    var book = widget.book;

    if (book['image'] != null) {
      String url = await storage.ref().child(book['image']).getDownloadURL();
      setState(() {
        CoverUrl = url;
      });

      print(CoverUrl);
    }
  }

  Widget build(BuildContext context) {
    return CoverUrl != ''
        ? Image(
            image: NetworkImage(CoverUrl),
            height: widget.height.roundToDouble(),
            width: widget.width.roundToDouble(),
            fit: BoxFit.fill)
        : Image(
            image: AssetImage('images/bookCover.jpg'),
            height: widget.height.roundToDouble(),
            width: widget.width.roundToDouble(),
            fit: BoxFit.fill);
  }
}
