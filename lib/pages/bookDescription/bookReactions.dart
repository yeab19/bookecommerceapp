import 'package:flutter/material.dart';
import './reactionInfo.dart';

class BookReactions extends StatefulWidget {
  BookReactions({Key key, @required this.Users});

  final Users;
  @override
  _BookReactionsState createState() => _BookReactionsState();
}

class _BookReactionsState extends State<BookReactions> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      child: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              itemCount: widget.Users.length,
              itemBuilder: (context, index) {
                return (ReactionInfo(
                  userid: widget.Users[index]['userId'],
                  type: widget.Users[index]['type'],
                ));
              }),
        ],
      ),
    ));
  }
}
