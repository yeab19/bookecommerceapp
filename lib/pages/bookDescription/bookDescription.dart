// import 'dart:html';

import 'package:bookecommerceapp/utils/customDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import '../../utils/constants.dart';
import '../../state.dart';
import '../../utils/requests.dart';
import '../readBook/readBook.dart';
import './reviewBook.dart';
import './coverImage.dart';
import '../profile/profilePicture.dart';

import '../profile/userInfo.dart';
import './bookAuthor.dart';
import './peopleReactionPage.dart';

var cartid = '';
var cart = {};
var comments = [];
var Author = {};

class BookDescription extends StatefulWidget {
  @override
  _BookDescriptionState createState() => _BookDescriptionState();
}

class _BookDescriptionState extends State<BookDescription>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  @override
  TabController _tabController;
  bool bookSaved = false;
  bool bookAdded = false;
  bool bookLiked = false;
  bool _isLoading = true;
  String CoverUrl = '';
  var storage = firebase_storage.FirebaseStorage.instance;

  void initState() {
    _tabController = new TabController(length: 3, vsync: this);
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);

    refresh(state != AppLifecycleState.resumed);
  }

  refresh(bool isBackground) async {
    if (!isBackground) {
      await getComments();
    }
  }

  void didChangeDependencies() async {
    await loadImage();
    await getcartforuser();
    await checkSaved();
    await checkLiked();
    await checkAddedtoCart();
    await getComments();
    await getAuthor();
    super.didChangeDependencies();
  }

  Future<void> loadImage() async {
    final Map book = ModalRoute.of(context).settings.arguments;
    if (book['image'] != null) {
      String url = await storage.ref().child(book['image']).getDownloadURL();
      CoverUrl = url;
      print(CoverUrl);
    }
  }

  Future<void> getAuthor() {
    final Map book = ModalRoute.of(context).settings.arguments;
    String token = Provider.of<store>(context, listen: false).getToken;
    // print("Here to get the author");
    if (book['createdBy'] != null) {
      var response = getUserDetails(book['createdBy'], token);
      response.then((data) {
        Author = data['data'];
        setState(() {
          _isLoading = false;
        });
      });
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  Future<void> getComments() {
    String token = Provider.of<store>(context, listen: false).getToken;
    final Map book = ModalRoute.of(context).settings.arguments;

    // print("Book id is $book['id']");
    var response = getCommentsForBook(book['id'], token);
    response.then((data) {
      if (data['status'] == "No comments found") {
        comments = [];
      } else {
        setState(() {
          comments = data['data'];
        });
      }
    });
  }

  react(String reactionName, String UserId, String BookId, String token) async {
    Map requestData = {};
    requestData['ReactionName'] = reactionName;
    requestData['UserId'] = UserId;
    requestData['BookId'] = BookId;

    if (reactionName == 'like') {
      setState(() {
        bookLiked = true;
      });
    }
    await React(requestData, token);
  }

  savebook(String bookId, String UserId, String token) async {
    Map requestData = {};
    requestData['bookId'] = bookId;
    requestData['userId'] = UserId;

    setState(() {
      bookSaved = true;
    });

    await SaveBook(requestData, token);
  }

  Future<void> checkAddedtoCart() {
    String UserId = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;
    final Map book = ModalRoute.of(context).settings.arguments;

    if (cart['Books'] != null && cart['Books'].contains(book['id'])) {
      bookAdded = true;
    }
  }

  Future<void> checkLiked() async {
    String UserId = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;
    final Map book = ModalRoute.of(context).settings.arguments;

    // if (book['LikedBy'] != null) {
    //   if (book['LikedBy'].contains(UserId)) {
    //     bookLiked = true;
    //   } else {
    //     bookLiked = false;
    //   }
    // } else {
    //   setState(() {
    //     bookLiked = false;
    //   });
    // }
    var response = await checkBookLiked(book['id'], UserId, token);
    print(response);

    setState(() {
      bookLiked = response;
    });
  }

  Future<void> checkSaved() async {
    String UserId = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;

    final Map book = ModalRoute.of(context).settings.arguments;

    var response = await checkBookSaved(book['id'], UserId, token);
    setState(() {
      bookSaved = response;
    });
  }

  addToCart(String BookId) {
    // String UserId = Provider.of<store>(context,listen:false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;

    var checker = true;
    for (int i = 0; i < cart['Books'].length; i++) {
      if (BookId == cart['Books'][i]) {
        checker = false;
      }
    }

    if (checker) {
      var response = addBooktoCart(BookId, cartid, token);
      response.then((data) {
        print(data);
      });
    }
  }

  Future<void> getcartforuser() {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    var response = getCartforUser(UserId, token);
    response.then((data) {
      print(data);
      if (data['status'] == "success") {
        cart = data['data'][0];
        cartid = data['data'][0]['id'];
      }
    });
  }

  Widget build(BuildContext context) {
    // print(Author['timestamp'].toString().substring(0,10));
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    print(width / 13.59);
    // print(width /
    //     10.2595); // final url = storage.ref().child('5fe6a86c0143411f449f7f19.jpg');
    // print(url.getDownloadURL());
    final Map book = ModalRoute.of(context).settings.arguments;

    final String UserId = Provider.of<store>(context, listen: false).getUserId;

    final String token = Provider.of<store>(context, listen: false).getToken;
    // firebase_storage.FirebaseStorage storage = firebase_storage.FirebaseStorage.instance;
    // print(book['Comments']);
    // var fullname = Author['firstname'] + " " + Author['lastname'];
    return Scaffold(
      body: !_isLoading
          ? SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: height / 68.3, bottom: 10.0),
                    decoration: BoxDecoration(color: bookDescColor),
                    width: MediaQuery.of(context).size.width,
                    // height: MediaQuery.of(context).size.height / 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: buttonColor,
                              size: 30.0,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                        Container(
                          padding: EdgeInsets.only(
                              top: height / 68.3, left: width / 13.59),
                          child: Row(
                            children: [
                              CoverImage(
                                book: book,
                                height: (height ~/ 2.8).toInt(),
                                width: (width / 2.555).toInt(),
                              ),
                              // : Container(
                              //     height: 244,
                              //     width: 161,
                              //     child: Icon(Icons.image, size: 100),
                              //   ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: width / 10.2595,
                                    top: (height / 68.3) * 4),
                                width: width / 2.057,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      book['title'],
                                      maxLines: 4,
                                      style: kBookTitleStyle,
                                    ),
                                    BookAuthor(book: book),
                                    Row(
                                      children: [
                                        InkWell(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) {
                                                        return PeopleReaction();
                                                      },
                                                      settings: RouteSettings(
                                                          arguments: book)));
                                            },
                                            child: Text('250',
                                                style: kNoofBooksStyle)),
                                        InkWell(
                                          onTap: () {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return CustomDialog(
                                                    bookId: book['id'],
                                                  );
                                                });
                                          },
                                          child: Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10),
                                            child: Icon(
                                              Icons.thumb_up_alt,
                                              color: Colors.blue,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text('4/5 ratings (250)',
                                        style: kNoofBooksStyle),
                                    Text("English", style: kNoofBooksStyle)
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 18.0),
                    child: TabBar(
                        labelColor: Colors.black,
                        unselectedLabelColor: subheaderColor,
                        labelStyle: ktabBarStyle,
                        unselectedLabelStyle: ktabBarStyle,
                        tabs: [
                          Text("Description"),
                          Text("Author"),
                          Text("Reviews")
                        ],
                        controller: _tabController),
                  ),
                  Container(
                    height: 270,
                    padding: EdgeInsets.only(top: 12.0),
                    child: TabBarView(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20.0),
                              child:
                                  Text('About Book', style: ktabBarViewStyle),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 30.0),
                              child: SingleChildScrollView(
                                child: Text(
                                  book['description'],
                                  style: kbookDescriptionStyle,
                                  maxLines: 3,
                                ),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 20.0),
                              child: Row(
                                children: [
                                  Text(
                                    "Tags:",
                                    style: ktabBarViewStyle,
                                  ),
                                  // Text(book['tags'][0]),
                                  Container(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Wrap(
                                      direction: Axis.horizontal,
                                      children: List.generate(
                                          book['tags'].length, (index) {
                                        return FilterChip(
                                          labelPadding: EdgeInsets.all(8.0),
                                          elevation: 10,
                                          label: Text(book['tags'][index]),
                                          backgroundColor: Colors.transparent,
                                          shadowColor: buttonColor,
                                          onSelected: (value) {},
                                        );
                                      }),
                                    ),
                                  ),

                                  // Text(.toString()),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 25.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      if (!bookLiked) {
                                        react(
                                            "like", UserId, book['id'], token);
                                      }
                                    },
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8.0),
                                          child: Icon(
                                            Icons.thumb_up_alt,
                                            color: bookLiked
                                                ? Colors.blue
                                                : Colors.grey,
                                            size: 34,
                                          ),
                                        ),
                                        // Padding(
                                        //   padding: const EdgeInsets.symmetric(
                                        //       horizontal: 8.0),
                                        //   child: Text("Like",
                                        //       style: kReactionStyle),
                                        // ),
                                      ],
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      if (!bookSaved) {
                                        savebook(book['id'], UserId, token);
                                      }
                                    },
                                    child: Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 8.0),
                                          child: Icon(
                                            Icons.bookmark,
                                            color: bookSaved
                                                ? Colors.blue
                                                : Colors.grey,
                                            size: 34,
                                          ),
                                        ),
                                        // Padding(
                                        //     padding: const EdgeInsets.symmetric(
                                        //         horizontal: 8.0),
                                        //     child: bookSaved == true
                                        //         ? Text("Saved",
                                        //             style: kReactionStyle)
                                        //         : Text(
                                        //             "save",
                                        //             style: kReactionStyle,
                                        //           )),
                                      ],
                                    ),
                                  ),
                                  Row(children: [
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0),
                                      child: Icon(
                                        Icons.share,
                                        color: Colors.grey,
                                        size: 34,
                                      ),
                                    ),
                                    // Padding(
                                    //   padding: const EdgeInsets.symmetric(
                                    //       horizontal: 8.0),
                                    //   child:
                                    //       Text("Share", style: kReactionStyle),
                                    // )
                                  ]),
                                ],
                              ),
                            ),
                            Container(
                                padding: EdgeInsets.only(top: 20.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: book['free'] == true
                                      ? [
                                          Container(
                                              padding: EdgeInsets.all(10.0),
                                              decoration: BoxDecoration(
                                                  color: Colors.transparent,
                                                  border: Border.all(
                                                      color: buttonColor)),
                                              child: Text(
                                                'Free',
                                                style: kPriceStyle,
                                              )),
                                          FlatButton(
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) {
                                                          return BookReader();
                                                        },
                                                        settings: RouteSettings(
                                                            arguments: book)));
                                              },
                                              color: buttonColor,
                                              textColor: Colors.white,
                                              child: Text("Read Now",
                                                  style: kAddtoCartStyle))
                                        ]
                                      : [
                                          Container(
                                              padding: EdgeInsets.all(10.0),
                                              decoration: BoxDecoration(
                                                  color: Colors.transparent,
                                                  border: Border.all(
                                                      color: buttonColor)),
                                              child: Text(
                                                book['price'],
                                                style: kPriceStyle,
                                              )),
                                          FlatButton(
                                              onPressed: () {
                                                addToCart(book['id']);
                                              },
                                              color: buttonColor,
                                              textColor: Colors.white,
                                              child: !bookAdded
                                                  ? Text("Add to Cart",
                                                      style: kAddtoCartStyle)
                                                  : Text("Added to cart"))
                                        ],
                                ))
                          ],
                        ),
                        Container(
                            height: 300,
                            child: Author != null
                                ? Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 18.0),
                                        child: ProfilePicture(
                                            user: Author,
                                            width: 72,
                                            height: 64,
                                            isCircular: true),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 18.0, left: 18.0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Author.isNotEmpty
                                                ? Text(
                                                    Author['firstname'] +
                                                        " " +
                                                        Author['lastname'],
                                                    style: ksettingtextStyle)
                                                : Offstage(),
                                            Author.isNotEmpty
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 12.0),
                                                    child: UserInfo(
                                                        user: Author,
                                                        isBookDesc: true),
                                                  )
                                                : Offstage(),
                                            Author.isNotEmpty
                                                ? Padding(
                                                    padding: EdgeInsets.only(
                                                        top: height / 8),
                                                    child: Text(
                                                      "Joined" +
                                                          " " +
                                                          Author['timestamp']
                                                              .toString()
                                                              .substring(0, 10),
                                                      style: ksettingtextStyle,
                                                    ),
                                                  )
                                                : Offstage()
                                          ],
                                        ),
                                      )
                                    ],
                                  )
                                : Offstage()),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 50.0),
                          child: SingleChildScrollView(
                            physics: ScrollPhysics(),
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  comments.isEmpty
                                      ? Center(
                                          child: Text(
                                              "No comments for this book."))
                                      : ListView.builder(
                                          // ShrinkWrappingViewport:true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemCount: comments.length,
                                          itemBuilder: (context, index) {
                                            var commentDate = comments[index]
                                                    ['timestamp']
                                                .substring(0, 10);
                                            print(MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                4.195);
                                            return Container(
                                                width: 300,
                                                padding: EdgeInsets.only(
                                                    left: 20.0, top: 15.0),
                                                child: Row(children: [
                                                  ProfilePicture(
                                                      user: Author,
                                                      height: 0,
                                                      width: 0,
                                                      isCircular: true,
                                                      radius: 40),
                                                  // radius: 10),
                                                  Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 18.0),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      left:
                                                                          5.0),
                                                              child: Text(
                                                                  comments[
                                                                          index]
                                                                      [
                                                                      'userName'],
                                                                  style:
                                                                      kcommentUsernameStyle),
                                                            ),
                                                            // Padding(
                                                            //   padding: EdgeInsets.only(
                                                            //       left: commentTimestampPadding(
                                                            //           context)),
                                                            //   child: Text(
                                                            //       commentDate,
                                                            //       style:
                                                            //           kcommentUsernameStyle),
                                                            // )
                                                          ],
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 18.0,
                                                                top: 5),
                                                        child: Text(
                                                          comments[index]
                                                              ['commentText'],
                                                          style:
                                                              kcommentBodyStyle,
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left:
                                                                    width / 20,
                                                                top: height /
                                                                    100),
                                                        child: Text(commentDate,
                                                            style:
                                                                kcommentUsernameStyle),
                                                      ),
                                                      Divider(
                                                          color: Colors.black)
                                                    ],
                                                  )
                                                ]));
                                          }),
                                  Padding(
                                    padding: EdgeInsets.only(top: 20),
                                    child: Material(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      child: MaterialButton(
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                                    return ReviewBook();
                                                  },
                                                  settings: RouteSettings(
                                                      arguments: book)));
                                        },
                                        color: buttonColor,
                                        child: Text("Add a review"),
                                      ),
                                    ),
                                  )
                                ]),
                          ),
                        )
                      ],
                      controller: _tabController,
                    ),
                  ),
                ],
              ),
            )
          : Center(
              child: Loading(
                indicator: BallPulseIndicator(),
                size: 100.0,
                color: Colors.pink,
              ),
            ),
    );
  }
}
