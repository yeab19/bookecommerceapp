import 'package:bookecommerceapp/state.dart';
import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';
import './bookReactions.dart';

class PeopleReaction extends StatefulWidget {
  @override
  _PeopleReactionState createState() => _PeopleReactionState();
}

class _PeopleReactionState extends State<PeopleReaction>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  Map Book = {};
  int allReactions = 0;
  int no_of_likes = 0;
  int no_of_love = 0;
  int no_of_great = 0;
  int no_of_wow = 0;

  List AllReactions = [];
  List likeReactions = [];
  List loveReactions = [];
  List greatReactions = [];
  List wowReactions = [];

  @override
  void initState() {
    _tabController = new TabController(length: 5, vsync: this);
  }

  void didChangeDependencies() async {
    await getStatus();
    await getBookDetails();
  }

  Future<void> getStatus() async {
    final Map book = ModalRoute.of(context).settings.arguments;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getBookStatus(book['id'], token);
    print(response);
    setState(() {
      no_of_great = response['data']['great'];
      no_of_likes = response['data']['likes'];
      no_of_wow = response['data']['wow'];
      no_of_love = response['data']['love'];
      allReactions = no_of_great + no_of_likes + no_of_wow + no_of_love;
    });
  }

  Future<void> getBookDetails() async {
    final Map book = ModalRoute.of(context).settings.arguments;
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = await getBookById(book['id'], token);
    if (response['status'] == "success") {
      setState(() {
        Book = response['data'];
        if (Book['LikedBy'] != null) {
          print("Book is liked");
          for (var user in Book['LikedBy']) {
            likeReactions.add({"type": "like", "userId": user});
          }
        }
        if (Book['LovedBy'] != null) {
          print("Book is loved");
          for (var user in Book['LovedBy']) {
            loveReactions.add({"type": "love", "userId": user});
          }
        }
        if (Book['GreatCount'] != null) {
          for (var user in Book['GreatCount']) {
            greatReactions.add({"type": "great", "userId": user});
          }
        }
        if (Book['WowCount'] != null) {
          for (var user in Book['WowCount']) {
            wowReactions.add({"type": "wow", "userId": user});
          }
        }
        AllReactions = [
          ...likeReactions,
          ...loveReactions,
          ...greatReactions,
          ...wowReactions
        ];
        // print("This is all reactions");
      });
    } else {
      Book = {};
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.arrow_back,
                    color: buttonColor,
                    size: 46,
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(left: 18.0, top: 10.0),
                  child: Text(
                    "Reactions",
                    style: kAuthorStyle,
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: TabBar(
                  labelColor: Colors.black,
                  unselectedLabelColor: subheaderColor,
                  labelStyle: ktabBarStyle,
                  unselectedLabelStyle: ktabBarStyle,
                  isScrollable: true,
                  tabs: [
                    Text("All $allReactions"),
                    Row(mainAxisSize: MainAxisSize.min, children: [
                      Text("$no_of_likes"),
                      CircleAvatar(
                        backgroundImage: AssetImage('images/Grouplike.png'),
                        radius: 12,
                      )
                      // Icon(Icons.thumb_up, color: Colors.blue)
                    ]),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("$no_of_love"),
                        CircleAvatar(
                          backgroundImage: AssetImage('images/Framelove.png'),
                          radius: 12,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("$no_of_great"),
                        CircleAvatar(
                          backgroundImage: AssetImage('images/Framegreat.png'),
                          radius: 12,
                        )
                      ],
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text("$no_of_wow"),
                        CircleAvatar(
                          backgroundImage: AssetImage('images/Groupwow.png'),
                          radius: 12,
                        )
                      ],
                    )
                  ],
                  controller: _tabController),
            ),
            Container(
              height: 370,
              child: TabBarView(
                controller: _tabController,
                children: [
                  Column(
                    children: [
                      allReactions == 0
                          ? Center(
                              child: Text("No reactions for this book"),
                            )
                          : BookReactions(Users: AllReactions)
                    ],
                  ),
                  Column(
                    children: [
                      no_of_likes == 0
                          ? Center(
                              child: Text("No likes for this book"),
                            )
                          : BookReactions(Users: likeReactions)
                    ],
                  ),
                  Column(
                    children: [
                      no_of_love == 0
                          ? Center(
                              child: Text("No Love reactions for this book"),
                            )
                          : BookReactions(Users: loveReactions)
                    ],
                  ),
                  Column(
                    children: [
                      no_of_great == 0
                          ? Center(
                              child: Text("No Great reactions for this book"),
                            )
                          : BookReactions(Users: greatReactions)
                    ],
                  ),
                  Column(
                    children: [
                      no_of_wow == 0
                          ? Center(
                              child: Text("No Wow reactions for this book"),
                            )
                          : BookReactions(Users: wowReactions)
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}
