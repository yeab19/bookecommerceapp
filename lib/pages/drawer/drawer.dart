import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../savedBooks/savedBooks.dart';
import '../settings/settings.dart';
import '../publish/publish.dart';
import '../library/libraryPage.dart';
import '../profile/myProfile.dart';

import 'package:provider/provider.dart';
import '../../state.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isDark = Provider.of<store>(context, listen: false).getisDarkTheme;

    return Padding(
      padding: const EdgeInsets.only(top: 66.0, bottom: 120.0),
      child: Center(
        child: Drawer(
          child: Container(
            width: 308,
            height: 580,
            decoration: BoxDecoration(color: isDark ? Colors.black : color),
            child: ListView(
              children: <Widget>[
                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.home, size: 30, color: iconColor),
                  ),
                  title: Text("Home"),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.library_books, size: 30, color: iconColor),
                  ),
                  title: Text("Library"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return LibraryPage();
                    }));
                  },
                ),
                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.bookmark, size: 30, color: iconColor),
                  ),
                  title: Text("Saved Books"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return SavedBooks();
                    }));
                  },
                ),
                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.file_upload, size: 30, color: iconColor),
                  ),
                  title: Text("Upload"),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PublishPage();
                    }));
                  },
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return MyProfile();
                    }));
                  },
                  child: ListTile(
                    leading: Container(
                      width: 48,
                      height: 48,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('images/background.jpg'),
                          )),
                    ),
                    title: Text("My profile"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 120.0),
                  child: ListTile(
                    leading: IconButton(
                      icon: Icon(Icons.settings, size: 30, color: iconColor),
                    ),
                    title: Text("Settings"),
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return SettingsPage();
                      }));
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
