import 'package:bookecommerceapp/utils/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../drawer/drawer.dart';
import './bodyContents.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {


  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MyDrawer(),
      body: BodyContent()
    );
  }

}
