import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart' as requests;
import '../searchPage/search.dart';
import '../searchPage/categories.dart';
import '../discoverNew/discoverNew.dart';
import '../popularBooks/popularBooks.dart';

import '../bookDescription/bookDescription.dart';
import '../bookDescription/bookAuthor.dart';

import '../allAuthors/allAuthors.dart';
import '../profile/profile.dart';
import '../profile/profilePicture.dart';
import '../bookDescription/coverImage.dart';
import '../categories/bookCategory.dart';
import '../profile/numberofBooks.dart';

List<bool> isSelected = [false, false];
List books = [];
List FreeBooks = [];
List PaidBooks = [];
List Authors = [];

var searchText = TextEditingController();

class BodyContent extends StatefulWidget {
  @override
  _BodyContentState createState() => _BodyContentState();
}

class _BodyContentState extends State<BodyContent> {
  @override
  // ignore: must_call_super
  static final GlobalKey<FormFieldState<String>> _searchKey =
      GlobalKey<FormFieldState<String>>();
  FocusNode _focus = new FocusNode();

  void initState() {
    getBooks();
    getFreeBooks();
    getPaidBooks();
    getauthors();
    searchText.addListener(_atFocus);
  }

  Future<void> getUser() async {
    final String token = Provider.of<store>(context, listen: false).getToken;
    String userid = Provider.of<store>(context, listen: false).getUserId;

    var response = await requests.getUserDetails(userid, token);
    if (response['status'] == 'success') {
      setState(() {
        Provider.of<store>(context, listen: false)
            .setFirstname(response['data']['firstname']);
        Provider.of<store>(context, listen: false)
            .setLastname(response['data']['lastname']);
        // user = response['data'];
      });
    }
  }

  void didChangeDependencies() async {
    await getUser();
  }

  void dispose() {
    searchText.dispose();
    super.dispose();
  }

  void _atFocus() {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      // searchText.dispose();
      return SearchBookPage();
    }));
    // FocusScope.of(context).unfocus();
    // searchText.clear();

    setState(() {
      FocusScope.of(context).unfocus();
      searchText.clear();
      // searchText.removeListener(() {});
      // searchText.dispose();
      // searchText = TextEditingController();
      // searchText.addListener(_atFocus);
    });
    // searchText.clear();
  }

  getauthors() {
    String token = Provider.of<store>(context, listen: false).getToken;

    var response = requests.getAuthors(token);
    response.then((data) {
      if (data['status'] == 'success') {
        Authors = data['data'];
        print(Authors);
      }
    });
  }

  getBooks() {
    var response = requests.getBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        setState(() {
          books = data['data'];
        });
      }
    });
  }

  getFreeBooks() {
    var response = requests.getFreeBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        setState(() {
          FreeBooks = data['data'];
        });
      }
    });
  }

  getPaidBooks() {
    var response = requests.getPaidBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        setState(() {
          PaidBooks = data['data'];
        });
      }
    });
  }

  Widget build(BuildContext context) {
    // print(Provider.of<store>(context,listen:false).getToken);

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    bool isDark = Provider.of<store>(context, listen: false).getisDarkTheme;

    String firstname = Provider.of<store>(context, listen: false).getfirstname;

    print(firstname);

    print((width / 20).toString() + " " + " is here");
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 680,
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 250,
                  //top background image
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('images/background.jpg'),
                      fit: BoxFit.values[3],
                    ),
                  ),
                  //display username of the user
                  child: Padding(
                    padding: const EdgeInsets.only(top: 71.0, left: 33.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Hey $firstname",
                              style: KTitleStyle,
                            ),
                            // drawer

                            IconButton(
                                icon: Icon(
                                  Icons.menu,
                                  color: Colors.white,
                                  size: 30,
                                ),
                                onPressed: () {
                                  Scaffold.of(context).openDrawer();
                                })
                          ],
                        ),
                        //search bar for books
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                          child: TextField(
                            // onChanged: (value) {

                            // },
                            // key: _searchKey,
                            // onTap: () {
                            //   Navigator.push(context,
                            //       MaterialPageRoute(builder: (context) {
                            //     FocusScope.of(context).unfocus();
                            //     return SearchBookPage();
                            //   }));
                            // },
                            // focusNode: _focus,
                            controller: searchText,
                            style: KSearchFieldStyle,
                            decoration: InputDecoration(
                                filled: true,
                                focusColor: Colors.transparent,
                                fillColor: Colors.white,
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                labelText:
                                    "Search by Title,author,genre and category",
                                prefixIcon: Icon(Icons.search),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0))),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                //
                Padding(
                  padding:
                      EdgeInsets.only(top: 192.0, left: leftPadding(context)),
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    padding: EdgeInsets.all(width / 20),
                    width: MediaQuery.of(context).size.width,
                    height: 580,
                    decoration: BoxDecoration(
                        color: !isDark ? Colors.white : Colors.black54,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            bottomLeft: Radius.circular(40))),

                    // Discover new books
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 13.0),
                              child: Text(
                                "Discover new",
                                style: KHeaderTextStyle,
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return DiscovernewPage();
                                }));
                              },
                              child: Text(
                                "See all",
                                style: KlinkStyle,
                              ),
                            )
                          ],
                        ),
                        Container(
                          width: 50,
                          padding: const EdgeInsets.only(top: 12.0),
                          child: Text(
                            'Read new books before other book lovers',
                            style: kSubHeaderStyle,
                            maxLines: 2,
                          ),
                        ),

                        //List of the new books
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: books.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return Container(
                                width: 200,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 23.0, vertical: 20.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  BookDescription(),
                                              settings: RouteSettings(
                                                  arguments: books[index])));
                                    },
                                    child: Column(
                                      // mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: <Widget>[
                                        CoverImage(
                                          book: books[index],
                                          height: 180,
                                          width: 137,
                                        ),
                                        // Image(image: AssetImage(
                                        //   'images/micheleObama.png',
                                        //
                                        // ),
                                        //   height: 180,
                                        //   width: 137,),
                                        BookAuthor(book: books[index]),
                                        Container(
                                          padding: EdgeInsets.only(left: 15.0),
                                          // width:250,
                                          child: Text(
                                            books[index]['title'],
                                            style: kBookTitleStyle,
                                            textAlign: TextAlign.start,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 4,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),

          //Categories of books
          Padding(
            padding: const EdgeInsets.only(left: 33.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Categories ',
                  style: KHeaderTextStyle,
                ),
                InkWell(
                  child: Text(
                    "See all",
                    style: KlinkStyle,
                  ),
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return CategoriesPage();
                    }));
                  },
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 33.0, top: 20),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Education");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1532012197267-da84d127e765?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8ZWR1Y2F0aW9ufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Education",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Health");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1485527172732-c00ba1bf8929?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Health",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Science Fiction");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1529969790834-4e32e2bb2032?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8c2NpJTIwZml8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Sci-Fi",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Crime");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1592772874383-d08932d29db7?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y3JpbWV8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Crime",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Mystery");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1606764979711-81266cf301bc?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTV8fG15c3Rlcnl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Mystery",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Romance");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1522033467113-b8db23657b96?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8cm9tYW5jZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Romance",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Thriller");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0),
                      child: Container(
                        alignment: Alignment.bottomLeft,
                        height: 166,
                        width: 152,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  'https://images.unsplash.com/photo-1610201945030-d049e825461c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fHRocmlsbGVyfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                              fit: BoxFit.fitWidth),
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Thriller",
                            style: kGenreStyle,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),

          //Popular Books(paid and free)
          Padding(
            padding: const EdgeInsets.only(left: 33.0, top: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Popular',
                  style: KHeaderTextStyle,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return PopularBooks();
                    }));
                  },
                  child: Text(
                    "See all",
                    style: KlinkStyle,
                  ),
                )
              ],
            ),
          ),
          ToggleButtons(
              children: [
                Row(
                  children: <Widget>[
                    Text("Paid"),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text("Free"),
                  ],
                )
              ],
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < isSelected.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      isSelected[buttonIndex] = !isSelected[buttonIndex];
                    } else {
                      isSelected[buttonIndex] = false;
                    }
                  }
                });
              },
              isSelected: isSelected),
          SizedBox(
              height: 400,
              child: (() {
                if (isSelected[0]) {
                  return ListView.builder(
                    itemCount: PaidBooks.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) {
                      return Container(
                        width: 200,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 23.0, vertical: 20.0),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BookDescription(),
                                      settings: RouteSettings(
                                          arguments: PaidBooks[index])));
                            },
                            child: Column(
                              // mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                CoverImage(
                                  book: PaidBooks[index],
                                  height: 180,
                                  width: 137,
                                ),
                                BookAuthor(book: PaidBooks[index]),
                                Container(
                                  padding: EdgeInsets.only(left: 15.0),
                                  // width:250,
                                  child: Text(
                                    PaidBooks[index]['title'],
                                    style: kBookTitleStyle,
                                    textAlign: TextAlign.start,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 4,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 15.0),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        PaidBooks[index]['price'],
                                        style: KlinkStyle,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
                } else {
                  return ListView.builder(
                      itemCount: FreeBooks.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return Container(
                          width: 200,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 23.0, vertical: 20.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => BookDescription(),
                                        settings: RouteSettings(
                                            arguments: FreeBooks[index])));
                              },
                              child: Column(
                                // mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  CoverImage(
                                    book: FreeBooks[index],
                                    height: 180,
                                    width: 137,
                                  ),
                                  Text(
                                    'Michele Obama',
                                    style: kBookAuthorStyle,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 15.0),
                                    // width:250,
                                    child: Text(
                                      FreeBooks[index]['title'],
                                      style: kBookTitleStyle,
                                      textAlign: TextAlign.start,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 4,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                }
              }())),

          //Authors to follow
          Padding(
            padding: EdgeInsets.only(left: 33.0, bottom: 12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Authors to follow",
                  style: KHeaderTextStyle,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return AllAuthors();
                    }));
                  },
                  child: Text(
                    "See all",
                    style: KlinkStyle,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 200,
            child: ListView.builder(
                itemCount: Authors.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  var firstname = Authors[index]['firstname'][0].toUpperCase() +
                      Authors[index]['firstname'].substring(1);
                  var lastname = Authors[index]['lastname'][0].toUpperCase() +
                      Authors[index]['lastname'].substring(1);
                  String fullname = firstname + " " + lastname;
                  return InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) {
                                return ProfilePage();
                              },
                              settings:
                                  RouteSettings(arguments: Authors[index])));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 38.0, left: 33.0),
                      child: Card(
                        color: !isDark ? Colors.white : Colors.black54,
                        elevation: 32.0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 22.0, horizontal: 10.0),
                          child: Row(
                            children: <Widget>[
                              ProfilePicture(
                                user: Authors[index],
                                width: 72,
                                height: 64,
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0),
                                child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text(
                                        fullname,
                                        style: kAuthorStyle,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 6.0),
                                            child: Icon(
                                              Icons.library_books,
                                              color: subheaderColor,
                                            ),
                                          ),
                                          NumberofBooks(user: Authors[index])
                                        ],
                                      )
                                    ]),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                }),
          ),
          // SingleChildScrollView(
          //   scrollDirection: Axis.horizontal,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //     children: <Widget>[
          //       Padding(
          //         padding: const EdgeInsets.only(bottom:38.0,left: 33.0),
          //         child: Card(
          //           elevation: 32.0,
          //           child: Padding(
          //             padding: const EdgeInsets.symmetric(vertical:22.0 ,horizontal:10.0 ),
          //             child: Row(
          //               children: <Widget>[
          //                 Image(
          //                   image: AssetImage('images/philipDick.jpg'),
          //                   width: 72,
          //                   height: 64,
          //                 ),
          //                 Padding(
          //                   padding: const EdgeInsets.symmetric(horizontal: 15.0),
          //                   child: Column(
          //                       mainAxisSize: MainAxisSize.min,
          //                       mainAxisAlignment: MainAxisAlignment.start,
          //                       children:[
          //                         Text(
          //                           "Philip Dick",
          //                           style: kAuthorStyle,
          //                         ),
          //                         Row(
          //                           children: <Widget>[
          //                             Padding(
          //                               padding: const EdgeInsets.symmetric(horizontal:6.0),
          //                               child: Icon(
          //                                 Icons.library_books,
          //                                 color:subheaderColor,
          //                               ),
          //                             ),
          //                             Text(
          //                               "45 books",
          //                               style: kNoofBooksStyle,
          //                             )
          //                           ],
          //                         )
          //                       ]
          //                   ),
          //                 )
          //               ],
          //
          //             ),
          //           ),
          //         ),
          //       ),
          //
          //       Padding(
          //         padding: const EdgeInsets.only(bottom:38.0,left: 33.0),
          //         child: Card(
          //           elevation: 32.0,
          //           child: Padding(
          //             padding: const EdgeInsets.symmetric(vertical:22.0 ,horizontal:10.0 ),
          //             child: Row(
          //               children: <Widget>[
          //                 Image(
          //                   image: AssetImage('images/JKRowling2.jpg'),
          //                   width: 72,
          //                   height: 64,
          //                 ),
          //                 Padding(
          //                   padding: const EdgeInsets.symmetric(horizontal: 15.0),
          //                   child: Column(
          //                       mainAxisSize: MainAxisSize.min,
          //                       mainAxisAlignment: MainAxisAlignment.start,
          //                       children:[
          //                         Text(
          //                           "JK Rowling",
          //                           style: kAuthorStyle,
          //                         ),
          //                         Row(
          //                           children: <Widget>[
          //                             Padding(
          //                               padding: const EdgeInsets.symmetric(horizontal:6.0),
          //                               child: Icon(
          //                                 Icons.library_books,
          //                                 color:subheaderColor,
          //                               ),
          //                             ),
          //                             Text(
          //                               "7 books",
          //                               style: kNoofBooksStyle,
          //                             )
          //                           ],
          //                         )
          //                       ]
          //                   ),
          //                 )
          //               ],
          //
          //             ),
          //           ),
          //         ),
          //       )
          //     ],
          //   ),
          // )
        ],
      ),
    );
  }
}
