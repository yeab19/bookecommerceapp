import 'package:bookecommerceapp/pages/savedBooks/cardDialog.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../bookDescription/bookDescription.dart';
import '../bookDescription/coverImage.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../bookDescription/bookAuthor.dart';

List<bool> isSelected = [false, false];
List savedBooks = [];
List cartBooks = [];
String cartId = '';
Map user = {};

class SavedBooks extends StatefulWidget {
  @override
  _SavedBooksState createState() => _SavedBooksState();
}

class _SavedBooksState extends State<SavedBooks> {
  bool isLoading = true;

  @override
  void initState() {}

  void didChangeDependencies() async {
    await getSavedBooks();
    await getcartforuser();
    await getUser();
    setState(() {
      isLoading = false;
    });
  }

  getUser() async {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    var response = await getUserDetails(UserId, token);
    if (response['status'] == 'success') {
      setState(() {
        user = response['data'];
      });
    }
  }

  checkBought(Map book) {
    print(user);
    if (user.isNotEmpty) {
      if (user['booksBought'].contains(book['id'])) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  getSavedBooks() async {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;
    var response = await getUsersSavedBooks(UserId, token);

    if (response['status'] == 'success') {
      setState(() {
        savedBooks = response['data'];
      });
    } else {
      setState(() {
        savedBooks = [];
      });
    }
  }

  getcartforuser() async {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    var response = await getCartforUser(UserId, token);
    if (response['status'] == "success") {
      cartId = response['data'][0]['id'];
      var cartresponse = await getBooksinCart(cartId, token);
      if (cartresponse['status'] == "success") {
        cartBooks = cartresponse['data'];
      }
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: !isLoading
            ? Container(
                padding: EdgeInsets.only(top: 30.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 12.0, right: 20.0),
                          child: IconButton(
                              icon: Icon(
                                Icons.arrow_back,
                                color: buttonColor,
                                size: 40.0,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                        ),
                        Text(
                          "Saved Books",
                          style: ktitleTextStyle,
                        )
                      ],
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 25.0, top: 10.0),
                      child: Row(
                        children: [
                          ToggleButtons(
                              renderBorder: false,
                              children: [
                                Container(
                                  width: 80,
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        "Saved",
                                        style: kBookTitleStyle,
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width: 80,
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                        "cart",
                                        style: kBookTitleStyle,
                                      ),
                                    ],
                                  ),
                                )
                              ],
                              onPressed: (int index) {
                                setState(() {
                                  for (int buttonIndex = 0;
                                      buttonIndex < isSelected.length;
                                      buttonIndex++) {
                                    if (buttonIndex == index) {
                                      isSelected[buttonIndex] =
                                          !isSelected[buttonIndex];
                                    } else {
                                      isSelected[buttonIndex] = false;
                                    }
                                  }
                                });
                              },
                              isSelected: isSelected),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.black,
                    ),
                    Container(
                      child: Expanded(
                        child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: !isSelected[1]
                                ? savedBooks.length
                                : cartBooks.length,
                            itemBuilder: (content, index) {
                              return Column(
                                children: [
                                  InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    BookDescription(),
                                                settings: RouteSettings(
                                                    arguments: !isSelected[1]
                                                        ? savedBooks[index]
                                                        : cartBooks[index])));
                                      },
                                      child: Container(
                                        padding: EdgeInsets.only(bottom: 20.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: [
                                            CoverImage(
                                              book: !isSelected[1]
                                                  ? savedBooks[index]
                                                  : cartBooks[index],
                                              height: 208,
                                              width: 137,
                                            ),
                                            Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                textBaseline:
                                                    TextBaseline.alphabetic,
                                                children: [
                                                  Container(
                                                    width: 200,
                                                    child: BookAuthor(
                                                        book: !isSelected[1]
                                                            ? savedBooks[index]
                                                            : cartBooks[index]),
                                                  ),
                                                  Container(
                                                    width: 200,
                                                    child: Text(!isSelected[1]
                                                        ? savedBooks[index]
                                                            ['title']
                                                        : cartBooks[index]
                                                            ['title']),
                                                  ),
                                                  FlatButton(
                                                      onPressed: () {},
                                                      color: buttonColor,
                                                      shape: RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10.0),
                                                          side: BorderSide(
                                                              color:
                                                                  buttonColor)),
                                                      child: !isSelected[1]
                                                          ? Text(
                                                              savedBooks[index]
                                                                      ['free']
                                                                  ? "Free"
                                                                  : savedBooks[
                                                                          index]
                                                                      ['price'])
                                                          : Text(cartBooks[index]
                                                                  ['free']
                                                              ? "Free"
                                                              : cartBooks[index]
                                                                  ['price'])),
                                                  isSelected[1]
                                                      ? FlatButton(
                                                          color: buttonColor,
                                                          shape: RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          10.0),
                                                              side: BorderSide(
                                                                  color:
                                                                      buttonColor)),
                                                          onPressed: () {
                                                            if (!checkBought(
                                                                cartBooks[
                                                                    index])) {
                                                              showDialog(
                                                                  context:
                                                                      context,
                                                                  builder:
                                                                      (context) {
                                                                    return CardDialog(
                                                                        id: cartBooks[index]
                                                                            [
                                                                            'id']);
                                                                  });
                                                            }
                                                          },
                                                          child: !checkBought(
                                                                  cartBooks[
                                                                      index])
                                                              ? Text("Buy Book")
                                                              : Text(
                                                                  "Bought book"))
                                                      : Offstage()
                                                ])
                                          ],
                                        ),
                                      )),
                                  Divider(
                                    color: Colors.black,
                                  )
                                ],
                              );
                            }),
                      ),
                    )
                  ],
                ),
              )
            : Text("Loading"));
  }
}
