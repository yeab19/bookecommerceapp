import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../bookDescription/bookDescription.dart';
import '../bookDescription/coverImage.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../bookDescription/bookAuthor.dart';

class CardDialog extends StatefulWidget {
  CardDialog({Key key, @required this.id});

  final String id;
  @override
  _CardDialogState createState() => _CardDialogState();
}

class _CardDialogState extends State<CardDialog> {
  var card = TextEditingController();
  bool successful = false;

  buy() async {
    var UserId = Provider.of<store>(context, listen: false).getUserId;
    var token = Provider.of<store>(context, listen: false).getToken;
    var id = widget.id;

    var response = await buywithStripe(UserId, id, card.text, token);
    if (response['status'] == "success") {
      setState(() {
        successful = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: height / 4),
        child: SingleChildScrollView(
          child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            elevation: 0,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10.0),
                    child: Text(
                      "Enter your card Details ",
                      style: ksettingStyle,
                    ),
                  ),
                  Container(
                    width: 200,
                    padding: EdgeInsets.only(top: 15.0),
                    child: TextField(
                      controller: card,
                    ),
                  ),
                  Container(
                    width: 200,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Icon(
                          Icons.credit_card,
                          color: Colors.orangeAccent,
                          size: 30,
                        ),
                        Icon(
                          Icons.payments_rounded,
                          color: Colors.orangeAccent,
                          size: 30,
                        )
                        // Icon(CupertinoIcons.mast)
                      ],
                    ),
                  ),
                  successful
                      ? Container(
                          width: 200,
                          child: Text(
                            "Successfully Bought the book",
                            style: TextStyle(color: Colors.green),
                          ),
                        )
                      : Offstage(),
                  Container(
                    width: 200,
                    padding: EdgeInsets.only(top: 15.0),
                    child: MaterialButton(
                      color: buttonColor,
                      onPressed: () {
                        buy();
                      },
                      child: Text("Buy Book"),
                    ),
                  ),
                  Container(
                    width: 200,
                    padding: EdgeInsets.only(top: 15.0),
                    child: MaterialButton(
                      color: buttonColor,
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      child: Text("Go Back"),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
