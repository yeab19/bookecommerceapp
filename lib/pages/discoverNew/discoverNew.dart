import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import '../bookDescription/bookAuthor.dart';
import '../bookDescription/bookDescription.dart';
import '../bookDescription/coverImage.dart';
import '../../utils/constants.dart';
import '../../utils/backButton.dart';
import '../../utils/requests.dart';

List Books = [];

class DiscovernewPage extends StatefulWidget {
  @override
  _DiscovernewPageState createState() => _DiscovernewPageState();
}

class _DiscovernewPageState extends State<DiscovernewPage> {
  @override
  bool isLoading = true;
  void initState() {}

  void didChangeDependencies() async {
    await getBooks();
  }

  Future<void> getBooks() async {
    var response = await getNewBooks();
    if (response['status'] == 'success') {
      setState(() {
        Books = response['data'];
        isLoading = false;
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: !isLoading
            ? Column(
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 12.0, right: 20.0),
                        child: IconButton(
                            icon: Icon(
                              Icons.arrow_back,
                              color: buttonColor,
                              size: 40.0,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            }),
                      ),
                      Text(
                        "Discover new",
                        style: ktitleTextStyle,
                      )
                    ],
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  Container(
                      child: Expanded(
                    child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: Books.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              BookDescription(),
                                          settings: RouteSettings(
                                              arguments: Books[index])));
                                },
                                child: Container(
                                  padding: EdgeInsets.only(bottom: 20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      CoverImage(
                                        book: Books[index],
                                        height: 208,
                                        width: 137,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        textBaseline: TextBaseline.alphabetic,
                                        children: [
                                          Container(
                                            width: 200,
                                            child:
                                                BookAuthor(book: Books[index]),
                                          ),
                                          Container(
                                              width: 200,
                                              child: Text(
                                                Books[index]['title'],
                                                style: kBookTitleHeaderStyle,
                                              )),
                                          Books[index]['free']
                                              ? FlatButton(
                                                  onPressed: () {},
                                                  child: Text("Free"),
                                                  color: buttonColor,
                                                )
                                              : FlatButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10.0),
                                                      side: BorderSide(
                                                          color: buttonColor)),
                                                  onPressed: () {},
                                                  child: Text(
                                                      Books[index]['price']),
                                                  color: buttonColor,
                                                )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Divider(
                                color: Colors.black,
                              )
                            ],
                          );
                        }),
                  )),
                ],
              )
            : Center(
                child: Loading(
                    indicator: BallPulseIndicator(),
                    size: 100.0,
                    color: Colors.pink),
              ),
      ),
    );
  }
}
