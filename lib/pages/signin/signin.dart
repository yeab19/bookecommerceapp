import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import '../../utils/requests.dart';
import '../homePage/homepage.dart';
import '../../utils/constants.dart';
import '../signup/signup.dart';

import 'package:provider/provider.dart';
import '../../state.dart';
import 'forgotPassword.dart';

final email = TextEditingController();
final password = TextEditingController();
bool obscureText = true;

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool isLoading = false;
  bool wronginput = false;

  @override
  SignIN(BuildContext context) {
    print(isLoading);
    var data = {'email': email.text, 'password': password.text};
    var response = login(data);
    response.then((Data) {
      setState(() {
        isLoading = false;
      });
      print(isLoading);
      if (Data['status'] == 'error') {
        print("error");
      } else if (Data['status'] == 'success') {
        Provider.of<store>(context, listen: false)
            .setUserId(Data['data']['userId']);
        Provider.of<store>(context, listen: false).setToken(Data['data']['id']);

        Provider.of<store>(context, listen: false).setLogged();
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return HomePage();
        }));
        setState(() {
          isLoading = false;
        });
      }
    });
    // print(jsonDecode(response['status']));
  }

  final emailField = TextField(
    controller: email,
    obscureText: false,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Enter Email",
        prefixIcon: Icon(Icons.email),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  final passwordField = TextField(
    controller: password,
    obscureText: obscureText,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Enter Password",
        prefixIcon: Icon(Icons.lock),
        suffixIcon: InkWell(
            onTap: () {
              print("hello");
              obscureText = !obscureText;
              // setState((){
              //   obscureText = !obscureText;
              // });
            },
            child: Icon(Icons.remove_red_eye)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Image(
                    image: AssetImage('images/magyz.png'),
                    width: 100,
                    height: 100,
                  )),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text(
                    "Sign in to your Account",
                    style: KSubTitleStyle,
                  )),
              Container(padding: EdgeInsets.all(10.0), child: emailField),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: password,
                    obscureText: obscureText,
                    style: KTextFieldStyle,
                    decoration: InputDecoration(
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        labelText: "Enter Password",
                        prefixIcon: Icon(Icons.lock),
                        suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                obscureText = !obscureText;
                              });
                            },
                            child: Icon(
                              Icons.remove_red_eye,
                              color: obscureText ? Colors.grey : Colors.blue,
                            )),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                  )),
              Container(
                padding: EdgeInsets.all(10.0),
                child: FractionallySizedBox(
                    widthFactor: 1,
                    child: Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: color,
                      child: MaterialButton(
                        // minWidth: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                        onPressed: () {
                          setState(() {
                            isLoading = true;
                            wronginput = false;
                          });

                          var data = {
                            'email': email.text,
                            'password': password.text
                          };
                          var response = login(data);
                          response.then((Data) {
                            print(Data);
                            if (Data['status'] == 'error') {
                              print("error");
                              setState(() {
                                isLoading = false;
                                wronginput = true;
                              });
                            } else if (Data['status'] == 'success') {
                              Provider.of<store>(context, listen: false)
                                  .setUserId(Data['data']['userId']);
                              Provider.of<store>(context, listen: false)
                                  .setToken(Data['data']['id']);
                              Provider.of<store>(context, listen: false)
                                  .setLogged();
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (context) {
                                return HomePage();
                              }));
                            }
                          });
                        },
                        // shape: ShapeBorder.,
                        color: buttonColor,
                        // shape: ,
                        child: Text(
                          "Sign In",
                          style: KTitleStyle,
                        ),
                      ),
                    )),
              ),
              Container(
                  height: wronginput ? 50.0 : 0.0,
                  padding: EdgeInsets.only(left: 10.0),
                  child: wronginput
                      ? Row(
                          children: [
                            Text("Incorrect username or password",
                                style: TextStyle(color: Colors.red)),
                          ],
                        )
                      : null),
              Container(
                  height: isLoading ? 50.0 : 0.0,
                  child: isLoading
                      ? Loading(
                          indicator: BallPulseIndicator(),
                          size: isLoading ? 100.0 : 0.0,
                          color: Colors.pink,
                        )
                      : null),
              InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return ForgotPassword();
                  }));
                },
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Forgot Password?",
                        style: KHeadlineStyle,
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Center(
                  child: Text("OR"),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Image(
                        image: AssetImage('images/facebook-logo.png'),
                        width: 50,
                        height: 50),
                    Image(
                        image: AssetImage('images/google-logo.png'),
                        width: 50,
                        height: 50),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text("I don't have an account"),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return SignUp();
                        }));
                      },
                      child: Container(
                          padding: EdgeInsets.only(left: 5.0),
                          child: Text(
                            "Sign up",
                            style: signupStyle,
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
