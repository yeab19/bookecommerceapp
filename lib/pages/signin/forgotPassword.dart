import 'package:bookecommerceapp/pages/signin/signin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';

final email = TextEditingController();

class ForgotPassword extends StatelessWidget {
  sendEmail(BuildContext context) async {
    var response = await resetPassword(email.text);
    print(response);
    if (response['status'] == 'success') {
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return SignIn();
      }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            padding: EdgeInsets.only(top: 33),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "Forgot Password",
                    style: kGenreStyle,
                  ),
                  Padding(
                    padding:
                        EdgeInsets.only(top: 40.0, left: 20.0, right: 20.0),
                    child: Text(
                        "If you have forgotten your password a link will be sent to your email address."),
                  ),
                  Center(
                    child: Container(
                        width: 200,
                        child: TextField(
                            controller: email,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                labelText: "Enter Email",
                                border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.circular(32.0))))),
                  ),
                  MaterialButton(
                    elevation: 10,
                    color: buttonColor,
                    onPressed: () {
                      sendEmail(context);
                    },
                    child: Text("Submit Email"),
                  )
                ],
              ),
            )));
  }
}
