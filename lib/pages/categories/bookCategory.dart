import 'package:bookecommerceapp/pages/bookDescription/bookDescription.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

import '../bookDescription/coverImage.dart';
import '../bookDescription/bookAuthor.dart';

class BookCategory extends StatefulWidget {
  BookCategory({Key key, @required this.category});

  final category;
  @override
  _BookCategoryState createState() => _BookCategoryState();
}

class _BookCategoryState extends State<BookCategory> {
  @override
  List Books = [];
  bool isLoading = true;

  void didChangeDependencies() async {
    await getBooks();
    setState(() {
      isLoading = false;
    });
  }

  getBooks() async {
    final token = Provider.of<store>(context, listen: false).getToken;

    var request = await getBookByCategory(widget.category, token);

    if (request['status'] == "success") {
      Books = request['data'];
    }
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: !isLoading
          ? Container(
              padding: EdgeInsets.only(top: height / 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            color: buttonColor,
                            size: 40.0,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(widget.category, style: kchatNameStyle),
                      )
                    ],
                  ),
                  Divider(color: Colors.black),
                  Books.length != 0
                      ? Container(
                          child: Expanded(
                            child: ListView.builder(
                                itemCount: Books.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return Container(
                                    padding: EdgeInsets.all(8.0),
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) {
                                                  return BookDescription();
                                                },
                                                settings: RouteSettings(
                                                    arguments: Books[index])));
                                      },
                                      child: Row(children: [
                                        CoverImage(
                                          book: Books[index],
                                          width: (width / 2.8).toInt(),
                                          height: (height / 3.5).toInt(),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10.0, left: 10.0),
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              textBaseline:
                                                  TextBaseline.alphabetic,
                                              children: [
                                                Container(
                                                  width: 200,
                                                  child: BookAuthor(
                                                      book: Books[index]),
                                                ),
                                                Container(
                                                  width: 200,
                                                  child: Text(
                                                      Books[index]['title']),
                                                ),
                                                FlatButton(
                                                    onPressed: () {},
                                                    color: buttonColor,
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10.0),
                                                        side: BorderSide(
                                                            color:
                                                                buttonColor)),
                                                    child: Text(Books[index]
                                                            ['free']
                                                        ? "Free"
                                                        : Books[index]
                                                            ['price']))
                                              ]),
                                        )
                                      ]),
                                    ),
                                  );
                                }),
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.only(left: 40, right: 10),
                          height: height / 1.2,
                          child: Center(
                              child: Text(
                                  "No Books in this Category.We will notify you when there are books in this category.")),
                        ),
                ],
              ),
            )
          : Container(
              height: height,
              child: Center(
                child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 100.0,
                  color: Colors.pink,
                ),
              ),
            ),
    );
  }
}
