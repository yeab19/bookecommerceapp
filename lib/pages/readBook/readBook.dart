import 'dart:io';

// import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

import '../../utils/constants.dart';

class BookReader extends StatefulWidget {
  @override
  _BookReaderState createState() => _BookReaderState();
}

class _BookReaderState extends State<BookReader> {
  bool _isLoading = true;
  bool _isBookRendered = false;
  // PDFDocument document ;
  String urlPath = '';
  // double _sliderValue = 0;
  int _totalPages = 1;
  int _currentPage = 0;
  String assetPath = 'images/sbb-31-02 cheque.pdf';
  PDFViewController _pdfViewController;

  @override
  void initState() {
    super.initState();
    getFileFromUrl("http://www.pdf995.com/samples/pdf.pdf").then((f) {
      setState(() {
        urlPath = f.path;
        _isLoading = false;
        print(urlPath);
      });
    });
    // loadDocument();
  }

  getFileFromUrl(String url) async {
    try {
      var data = await http.get(url);
      var bytes = data.bodyBytes;
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/mypdfonline.pdf");

      File urlFile = await file.writeAsBytes(bytes);
      return urlFile;
    } catch (e) {
      throw Exception("error reading file");
    }
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    print(height / 1.2);
    return Scaffold(
      body: _isLoading
          ? Container(
              padding: EdgeInsets.only(top: height / 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 20.0),
                        child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back,
                            color: Colors.black,
                            size: 38.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                      height: _isLoading ? 50.0 : 0.0,
                      child: _isLoading
                          ? Center(
                              child: Loading(
                                indicator: BallPulseIndicator(),
                                size: _isLoading ? 100.0 : 0.0,
                                color: Colors.pink,
                              ),
                            )
                          : Offstage()),
                ],
              ))
          : Container(
              padding: EdgeInsets.only(top: height / 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        padding: EdgeInsets.only(right: 20.0),
                        child: IconButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          icon: Icon(
                            Icons.arrow_back,
                            color: Colors.black,
                            size: 38.0,
                          ),
                        ),
                      ),
                      Container(
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                            Icon(Icons.menu),
                            Text("Aa"),
                            Icon(Icons.more_vert)
                          ]))
                    ],
                  ),
                  Container(
                    color: color,
                    height: height / 1.3,
                    width: MediaQuery.of(context).size.width,
                    child: PDFView(
                      fitPolicy: FitPolicy.WIDTH,
                      filePath: urlPath,
                      autoSpacing: true,
                      enableSwipe: true,
                      pageSnap: true,
                      onError: (e) {
                        print(e);
                      },
                      onRender: (_pages) {
                        _totalPages = _pages;
                        setState(() {
                          _isBookRendered = true;
                        });
                      },
                      onViewCreated: (PDFViewController vc) {
                        _pdfViewController = vc;
                      },
                      onPageChanged: (int page, int total) {
                        setState(() {});
                      },
                      onPageError: (page, e) {},
                    ),
                  ),
                  Container(
                    width: width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _currentPage > 0
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (_currentPage > 0) {
                                      _currentPage -= 1;
                                    }
                                  });
                                },
                                icon: Icon(Icons.chevron_left, size: 40))
                            : Offstage(),
                        Container(
                          width: width / 1.357,
                          child: Slider(
                              min: 0,
                              divisions: _totalPages,
                              activeColor: const Color(0xffEFC300),
                              max: _totalPages.roundToDouble(),
                              value: _currentPage.roundToDouble(),
                              onChanged: (value) {
                                setState(() {
                                  _currentPage = value.toInt();
                                  _pdfViewController.setPage(_currentPage);
                                });
                              }),
                        ),
                        _currentPage < _totalPages
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (_currentPage < _totalPages) {
                                      _currentPage += 1;
                                    }
                                  });
                                },
                                icon: Icon(Icons.chevron_right,
                                    size: 40, color: Colors.black87))
                            : Offstage()
                      ],
                    ),
                  )
                  // Row(
                  //   mainAxisAlignment:MainAxisAlignment.end,
                  //   children:[
                  //     _currentPage>0?FloatingActionButton.extended(
                  //         backgroundColor:Colors.red,
                  //         label:Text("Go the Previous Page"),
                  //         onPressed: (){
                  //           _currentPage -= 1;
                  //           _pdfViewController.setPage(_currentPage);
                  //         }):Offstage(),
                  //     _currentPage<_totalPages?FloatingActionButton.extended(
                  //         backgroundColor:Colors.green,
                  //         label:Text("Go the Next Page"),
                  //         onPressed: (){
                  //           _currentPage += 1;
                  //           _pdfViewController.setPage(_currentPage);
                  //         }):Offstage( )
                  //   ]
                  // )
                ],
              )),
    );
  }
}
