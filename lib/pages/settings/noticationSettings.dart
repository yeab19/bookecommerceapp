import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class NotificationSettings extends StatefulWidget {
  @override
  _NotificationSettingsState createState() => _NotificationSettingsState();
}

class _NotificationSettingsState extends State<NotificationSettings> {
  @override

  bool _inboxCheck = true;
  bool _reviewCheck = true;
  bool _replyCheck = true;
  bool _followerCheck = true;
  bool _updateCheck = true;

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          padding: EdgeInsets.only(top:35.0,left:12.0),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 40.0),
                    child: IconButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                        size: 38.0,
                      ),
                    ),
                  ),
                  Text(
                    "Notifications",
                    style: ksettingheaderStyle,
                  ),

                ],
              ),
              Divider(
                color: Colors.black,
              ),
              ListView(
                shrinkWrap: true,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:12.0),
                    child: Text(
                        "Messages",
                        style: ksettingheaderStyle,
                    ),
                  ),
                 CheckboxListTile(
                   title: Text(
                       "Inbox",
                      style: ksettingtextStyle,
                   ),
                   value:_inboxCheck,
                   activeColor: buttonColor,
                   onChanged: (val){
                     setState(() {
                       _inboxCheck = val;
                     });
                   },
                 ),
                  Divider(
                    color: Colors.black,
                  ),
                  CheckboxListTile(
                    title: Text(
                        "Reviews on my posts",
                      style: ksettingtextStyle,
                    ),
                    value:_reviewCheck,
                    activeColor: buttonColor,
                    onChanged: (val){
                      setState(() {
                        _reviewCheck = val;
                      });
                    },
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  CheckboxListTile(
                    title: Text(
                        "Replies to my reviews",
                      style: ksettingtextStyle,
                    ),
                    value:_replyCheck,
                    activeColor: buttonColor,
                    onChanged: (val){
                      setState(() {
                        _replyCheck = val;
                      });
                    },
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  CheckboxListTile(
                    title: Text(
                        "New followers",
                      style: ksettingtextStyle,
                    ),
                    value:_followerCheck,
                    activeColor: buttonColor,
                    onChanged: (val){
                      setState(() {
                        _followerCheck = val;
                      });
                    },
                  ),
                  Divider(
                    color: Colors.black,
                  ),
                  CheckboxListTile(
                    title: Text(
                        "Updates from Profiles I follow",
                      style: ksettingtextStyle,
                    ),
                    value:_updateCheck,
                    // checkColor: buttonColor,
                    activeColor: buttonColor,
                    onChanged: (val){
                      setState(() {
                        _updateCheck = val;
                      });
                    },
                  ),
                ],
              )
            ],
          ),
        ));
  }
}
