import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
      height: height,
      padding: EdgeInsets.only(top: 33),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 38.0,
                  ),
                ),
              ),
              Text(
                "About Us",
                style: kAboutStyle,
              )
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: height / 10),
            child: Image(image: AssetImage('images/magyz.png')),
          ),
          Text("Magyz", style: kNameStyle),
          InkWell(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(top: 48.0, left: 18.0),
              child: Row(
                children: [
                  Image(
                    image: AssetImage('images/facebook-logo.png'),
                    width: 34,
                    height: 34,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Follow us on Facebook",
                      style: kWorkStyle,
                    ),
                  )
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {},
            child: Padding(
              padding: const EdgeInsets.only(top: 18.0, left: 18.0),
              child: Row(
                children: [
                  Image(
                    image: AssetImage('images/facebook-logo.png'),
                    width: 34,
                    height: 34,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("Follow us on Twitter", style: kWorkStyle),
                  )
                ],
              ),
            ),
          ),
          Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                  alignment: Alignment.bottomCenter,
                  child: Text("Copyright . Magyz All rights reserved.")),
            ),
          )
        ],
      ),
    ));
  }
}
