import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';
import './landscapeDialog.dart';
import './readingMode.dart';

class ReadingPreferences extends StatefulWidget {
  @override
  _ReadingPreferencesState createState() => _ReadingPreferencesState();
}

class _ReadingPreferencesState extends State<ReadingPreferences> {
  @override
  bool _deviceBrightnessCheck = true;

  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: 35.0, left: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              BackButton(),
              Padding(
                padding: EdgeInsets.only(left: 20.0),
                child: Text(
                  "Reading Preferences",
                  style: kAuthorStyle,
                ),
              ),
            ],
          ),
          Divider(
            color: Colors.black,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0),
            child: InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return OrientationDialog();
                    });
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Screen Orientation',
                    style: ksettingTitleStyle,
                  ),
                  Text('Auto', style: kWorkStyle)
                ],
              ),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Padding(
            padding: EdgeInsets.only(left: 20.0),
            child: InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return ReadingmodeDialog();
                    });
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Reading Mode',
                    style: ksettingTitleStyle,
                  ),
                  Text('Paging', style: kWorkStyle)
                ],
              ),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          ListView(shrinkWrap: true, children: [
            CheckboxListTile(
              title: Text(
                "Use deviceBrightness",
                style: ksettingtextStyle,
              ),
              value: _deviceBrightnessCheck,
              activeColor: buttonColor,
              onChanged: (val) {
                setState(() {
                  _deviceBrightnessCheck = val;
                });
              },
            ),
          ]),
        ],
      ),
    ));
  }
}
