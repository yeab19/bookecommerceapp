import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class HelpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: 35.0, left: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 38.0,
                  ),
                ),
              ),
              Text("Help", style: ksettingheaderStyle)
            ],
          ),
          Divider(color: Colors.black),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "What do you need help with?",
              style: kuseragreementStyle,
            ),
          ),
          Padding(padding: EdgeInsets.all(8.0), child: Text("Help Center")),
          Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquet id "
                  "ullamcorper arcu non sit natoque sit pellentesque maecenas. Sed "
                  "posuere mattis elit metus at duis ut. Nunc tortor, imperdiet nunc "
                  "a commodo, nisl porta sed bibendum.")),
        ],
      ),
    ));
  }
}
