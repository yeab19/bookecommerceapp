import 'package:flutter/material.dart';

class ReadingmodeDialog extends StatefulWidget {
  @override
  _ReadingmodeDialogState createState() => _ReadingmodeDialogState();
}

class _ReadingmodeDialogState extends State<ReadingmodeDialog> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    bool pagingCheck = false;
    bool scrollingCheck = false;

    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: height / 5, bottom: height / 3),
        child: Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 0,
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                child: Text('Reading Mode'),
              ),
              CheckboxListTile(
                  value: pagingCheck,
                  title: Text("Paging"),
                  onChanged: (bool value) {
                    setState(() {
                      pagingCheck = value;
                    });
                  }),
              CheckboxListTile(
                  title: Text("Scroling"),
                  value: scrollingCheck,
                  onChanged: (bool value) {
                    setState(() {
                      if (scrollingCheck) {
                        scrollingCheck = true;
                      } else {
                        scrollingCheck = false;
                      }
                    });
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cancel"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
