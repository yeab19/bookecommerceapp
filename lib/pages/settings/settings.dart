import 'package:bookecommerceapp/pages/settings/darkmodeSwitch.dart';
import 'package:bookecommerceapp/pages/settings/helpPage.dart';
import 'package:bookecommerceapp/pages/settings/languageDialog.dart';
import 'package:bookecommerceapp/pages/settings/noticationSettings.dart';
import 'package:bookecommerceapp/pages/settings/privacyPolicy.dart';
import 'package:bookecommerceapp/pages/settings/userAgreement.dart';
import 'package:bookecommerceapp/pages/settings/readingPreferences.dart';

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import '../../state.dart';

import '../signin/signin.dart';
import '../../utils/constants.dart';
import 'profile.dart';
import 'aboutUs.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
          padding: EdgeInsets.only(top: 35.0, left: 12.0),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 20.0),
                    child: IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                        size: 40.0,
                      ),
                    ),
                  ),
                  Text(
                    "Settings",
                    style: ktitleTextStyle,
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(left: 12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        Text("Application Settings",
                            style: kBookTitleHeaderStyle),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ProfilePage();
                        }));
                      },
                      child: Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: Text(
                              "Account Settings",
                              style: ksettingStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return NotificationSettings();
                        }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          children: [
                            Text(
                              "Notifications",
                              style: ksettingStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ReadingPreferences();
                        }));
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: Row(
                          children: [
                            Text(
                              "Reading preference",
                              style: ksettingStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return DarkmodeSwitch();
                        }));
                      },
                      child: Container(
                        child: Row(
                          children: [
                            Text(
                              "Dark Mode",
                              style: ksettingStyle,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          Provider.of<store>(context).getisDarkTheme
                              ? "On"
                              : "Off",
                          style: ksettingStyle,
                        ),
                      ],
                    ),
                    Divider(color: Colors.black),
                    Row(
                      children: [
                        Text(
                          "Preffered Story",
                          style: kBookTitleHeaderStyle,
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return LanguageDialog();
                            });
                      },
                      child: Row(
                        children: [
                          Text(
                            "Language",
                            style: ksettingStyle,
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          "English",
                          style: ksettingStyle,
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "Others",
                          style: kBookTitleHeaderStyle,
                        ),
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return AboutUs();
                        }));
                      },
                      child: Row(
                        children: [
                          Text(
                            "About us",
                            style: ksettingStyle,
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return PrivacyPolicy();
                          return PrivacyPolicy();
                        }));
                      },
                      child: Row(
                        children: [
                          Text("Privacy policy", style: ksettingStyle),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return UserAgreement();
                        }));
                      },
                      child: Row(
                        children: [
                          Text("User agreement", style: ksettingStyle),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HelpPage();
                        }));
                      },
                      child: Row(
                        children: [
                          Text(
                            "Help",
                            style: ksettingStyle,
                          ),
                        ],
                      ),
                    ),
                    Divider(color: Colors.black),
                    InkWell(
                      onTap: () {
                        Provider.of<store>(context, listen: false).setLogged();
                        Provider.of<store>(context, listen: false).setToken('');
                        Provider.of<store>(context, listen: false)
                            .setUserId('');
                        Provider.of<store>(context, listen: false)
                            .setLastname('');
                        Provider.of<store>(context, listen: false)
                            .setFirstname('');

                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return SignIn();
                        }));
                      },
                      child: Row(
                        children: [
                          Text(
                            "Sign out",
                            style: ksettingStyle,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          )),
    ));
  }
}
