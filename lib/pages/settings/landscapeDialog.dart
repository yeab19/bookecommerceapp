import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class OrientationDialog extends StatefulWidget {
  @override
  _OrientationDialogState createState() => _OrientationDialogState();
}

class _OrientationDialogState extends State<OrientationDialog> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    bool autoCheck = false;
    bool landscapeCheck = false;
    bool portraitCheck = false;

    return Container(
      child: Padding(
        padding: EdgeInsets.only(top: height / 5, bottom: height / 3),
        child: Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 0,
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                child: Text('Screen Orientation'),
              ),
              CheckboxListTile(
                  value: autoCheck, title: Text("Auto"), onChanged: (value) {}),
              CheckboxListTile(
                  title: Text("Portrait"),
                  value: portraitCheck,
                  onChanged: (value) {}),
              CheckboxListTile(
                  title: Text("Landscape"),
                  value: landscapeCheck,
                  onChanged: (value) {}),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  MaterialButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cancel"),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
