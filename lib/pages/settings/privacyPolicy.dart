import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  bool _termsCheck = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: 35.0, left: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 38.0,
                  ),
                ),
              ),
              Text("Privacy Policy", style: ksettingheaderStyle)
            ],
          ),
          Divider(color: Colors.black),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquet id "
              "ullamcorper arcu non sit natoque sit pellentesque maecenas. Sed "
              "posuere mattis elit metus at duis ut. Nunc tortor, imperdiet nunc "
              "a commodo, nisl porta sed bibendum. Volutpat tincidunt senectus cursus"
              " tortor feugiat. Phasellus est nisi pellentesque vulputate odio. "
              "Enim amet sapien turpis volutpat. Vel suspendisse faucibus enim donec "
              "blandit. Quisque sagittis eu ullamcorper in orci, eu. Quis at vitae sed "
              "pulvinar. Ut arcu ullamcorper odio sagittis massa vitae aliquet nunc "
              "ultricies. Malesuada dui amet, nulla elit velit justo, non consectetur. "
              "Eget elit etiam.",
              style: kuseragreementStyle,
            ),
          ),
        ],
      ),
    ));
  }
}
