import 'package:bookecommerceapp/pages/settings/settings.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:loading/loading.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../profile/profilePicture.dart';
import '../chat/chatPage.dart';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Map user = {};
var firstname = TextEditingController();
var lastname = TextEditingController();
var email = TextEditingController();
var password = TextEditingController();

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  String fileName;
  String path;
  Map<String, String> paths;
  List<String> extensions;
  bool isLoadingPath = false;
  bool isMultiPick = false;
  FileType fileType = FileType.image;
  FilePickerResult result;
  PlatformFile image;

  void initState() {
    getuserdetails();
  }

  void _openFileExplorer() async {
    setState(() => isLoadingPath = true);
    try {
      // if (isMultiPick) {
      //   path = null;
      //   paths = await FilePicker.getMultiFilePath(type: fileType? fileType: FileType.any, allowedExtensions: extensions);
      // }
      // else {
      result = await FilePicker.platform
          .pickFiles(type: fileType, allowedExtensions: extensions);

      image = result.files.first;

      print(image.name);

      paths = null;
      // }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoadingPath = false;
      fileName = path != null
          ? path.split('/').last
          : paths != null
              ? paths.keys.toString()
              : '...';
    });
  }

  void getuserdetails() {
    var UserId = Provider.of<store>(context, listen: false).getUserId;
    var token = Provider.of<store>(context, listen: false).getToken;

    var response = getUserDetails(UserId, token);
    response.then((data) {
      setState(() {
        user = data['data'];
        firstname.text = user['firstname'];
        lastname.text = user['lastname'];
        email.text = user['email'];

        password.text = "******";
      });
    });
  }

  void changeImage() async {
    var UserId = Provider.of<store>(context, listen: false).getUserId;
    var token = Provider.of<store>(context, listen: false).getToken;

    setState(() {
      isLoadingPath = true;
    });
    var response = await uploadUserImage(UserId, image.path, token);
    await getuserdetails();
    setState(() {
      isLoadingPath = false;
    });
  }

  void editDetails() async {
    var UserId = Provider.of<store>(context, listen: false).getUserId;
    var token = Provider.of<store>(context, listen: false).getToken;

    var response = await editUserDetails(
        firstname.text, lastname.text, email.text, UserId, token);
    if (response['status'] == "success") {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SuccessDialog(isSuccess: true);
          });
    }
    // print(response);
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: EdgeInsets.only(top: 35.0, left: 12.0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 20.0),
                  child: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                      size: 48.0,
                    ),
                  ),
                ),
                Text(
                  "Account Setings",
                  style: ktitleTextStyle,
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(left: 18),
              child: Row(
                children: [
                  Text(
                    "My Profile",
                    style: ksettingheaderStyle,
                  )
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 18, top: 10.0),
              child: Row(
                children: [
                  InkWell(
                    onTap: () {
                      _openFileExplorer();
                    },
                    child: ProfilePicture(
                        user: user, width: 72, height: 64, isCircular: true),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, top: 5.0),
                    child: Column(
                      children: [
                        Text(
                          "Profile Picture",
                          style: ksettingheaderStyle,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Text("Tap to change", style: ksettingStyle),
                        ),
                        image != null
                            ? Container(
                                width: 100,
                                child: Text(
                                  image.name,
                                  maxLines: 3,
                                ),
                              )
                            : Offstage(),
                        isLoadingPath
                            ? Loading(color: Colors.black, size: 20)
                            : Offstage(),
                        image != null
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  MaterialButton(
                                      color: buttonColor,
                                      onPressed: () {
                                        changeImage();
                                      },
                                      child: Icon(Icons.edit)),
                                ],
                              )
                            : Offstage()
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Divider(
                color: Colors.black,
              ),
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                textDirection: TextDirection.ltr,
                textBaseline: TextBaseline.alphabetic,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 5.0),
                    child: Row(
                      children: [
                        Text(
                          "About",
                          style: ksettingheaderStyle,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 5.0),
                    child: Row(
                      children: [
                        Text(
                          "Brief Description",
                          style: ksettingheaderStyle,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15.0, top: 5.0),
                    child: Row(
                      children: [
                        Text(
                          "I am passionate writer, I love animals",
                          style: ksettingStyle,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Divider(
                color: Colors.black,
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "First Name",
                        style: ksettingheaderStyle,
                      )
                    ],
                  ),
                  Row(children: [
                    Container(
                        width: 200,
                        child: TextField(
                          controller: firstname,
                        ))
                  ])
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Last Name",
                        style: ksettingheaderStyle,
                      )
                    ],
                  ),
                  Row(children: [
                    Container(
                        width: 200,
                        child: TextField(
                          controller: lastname,
                        ))
                  ])
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Email",
                        style: ksettingheaderStyle,
                      )
                    ],
                  ),
                  Row(children: [
                    Container(
                        width: 200,
                        child: TextField(
                          controller: email,
                        ))
                  ])
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 12.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "Password",
                        style: ksettingheaderStyle,
                      )
                    ],
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: subheaderColor)),
                            width: 200,
                            child: InkWell(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (context) {
                                        return PasswordDialog();
                                      });
                                },
                                child: Text("Click and edit password"))),
                        Padding(
                          padding: const EdgeInsets.only(right: 10.0),
                          child: Material(
                            elevation: 5.0,
                            color: buttonColor,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            child: MaterialButton(
                              onPressed: () {
                                editDetails();
                              },
                              padding:
                                  EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                              color: buttonColor,
                              child: Text(
                                "Save",
                                style: ktitleTextStyle,
                              ),
                            ),
                          ),
                        )
                      ])
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}

class SuccessDialog extends StatelessWidget {
  SuccessDialog({Key key, @required this.isSuccess});

  final bool isSuccess;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.symmetric(vertical: height / 4),
      child: Dialog(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
        elevation: 0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(
              child: this.isSuccess
                  ? Text(
                      "Edited your credentials.",
                      style: ksettingStyle,
                    )
                  : Text(
                      "An error occured.",
                      style: ksettingStyle,
                    ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SettingsPage();
                }));
              },
              child: Text("Back to Settings"),
              color: buttonColor,
            ),
            MaterialButton(
                padding: EdgeInsets.symmetric(horizontal: 40),
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Cancel"),
                color: buttonColor)
          ],
        ),
      ),
    );
  }
}

class PasswordDialog extends StatefulWidget {
  // final bool isSuccess;
  @override
  _PasswordDialogState createState() => _PasswordDialogState();
}

class _PasswordDialogState extends State<PasswordDialog> {
  @override
  var password = TextEditingController();
  bool successful = false;

  edit() async {
    String token = Provider.of<store>(context, listen: false).getToken;
    var response = await editPassword(password.text, token);

    print(response);
    if (response['status'] == "success") {
      setState(() {
        successful = true;
      });
    }
  }

  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Container(
      padding: EdgeInsets.symmetric(vertical: height / 4),
      child: SingleChildScrollView(
        child: Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          elevation: 0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Center(
                  child: Text(
                "Edit your Password",
                style: ksettingStyle,
              )),
              Center(
                child: Container(
                  width: 200,
                  child: TextField(
                    controller: password,
                    decoration: InputDecoration(
                      labelText: "Enter your new password",
                    ),
                  ),
                ),
              ),
              successful
                  ? Center(
                      child: Container(
                        width: 200,
                        child: Text(
                          "SUccessfully changed the password",
                          style: TextStyle(color: Colors.green),
                        ),
                      ),
                    )
                  : Offstage(),
              MaterialButton(
                onPressed: () {
                  edit();
                },
                child: Text("Save Password"),
                color: buttonColor,
              ),
              MaterialButton(
                  padding: EdgeInsets.symmetric(horizontal: 40),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel"),
                  color: buttonColor)
            ],
          ),
        ),
      ),
    );
  }
}
