import 'package:flutter/material.dart';

class LanguageDialog extends StatefulWidget {
  @override
  _LanguageDialogState createState() => _LanguageDialogState();
}

class _LanguageDialogState extends State<LanguageDialog> {
  @override
  bool afrikaansCheck = false;
  bool bahasaCheck = false;
  bool englishCheck = false;
  bool frenchCheck = false;
  bool germanCheck = false;

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      child: Padding(
          padding: EdgeInsets.only(top: height / 5, bottom: height / 4),
          child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            elevation: 0,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                  child: Text('Language'),
                ),
                CheckboxListTile(
                  value: afrikaansCheck,
                  onChanged: (bool value) {
                    setState(() {
                      afrikaansCheck = value;
                    });
                  },
                  title: Text('Afrikaans'),
                ),
                CheckboxListTile(
                  value: bahasaCheck,
                  onChanged: (bool value) {
                    setState(() {
                      bahasaCheck = value;
                    });
                  },
                  title: Text('Bahasa Indonesia'),
                ),
                CheckboxListTile(
                  value: englishCheck,
                  onChanged: (bool value) {
                    setState(() {
                      englishCheck = value;
                    });
                  },
                  title: Text('English'),
                ),
                CheckboxListTile(
                  value: frenchCheck,
                  onChanged: (bool value) {
                    setState(() {
                      frenchCheck = value;
                    });
                  },
                  title: Text('French'),
                ),
                CheckboxListTile(
                  value: germanCheck,
                  onChanged: (bool value) {
                    setState(() {
                      germanCheck = value;
                    });
                  },
                  title: Text('German'),
                )
              ],
            ),
          )),
    );
  }
}
