import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../state.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

class DarkmodeSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.only(top: 35.0, left: 12.0),
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(right: 20.0),
                child: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                    size: 45.0,
                  ),
                ),
              ),
              Text("Dark Mode", style: ksettingheaderStyle)
            ],
          ),
          Divider(color: Colors.black),
          ListView(
            shrinkWrap: true,
            children: [
              InkWell(
                onTap: () {
                  Provider.of<store>(context, listen: false).setDark(true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text(
                    "On",
                    style: ksettingheaderStyle,
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
              ),
              InkWell(
                onTap: () {
                  Provider.of<store>(context, listen: false).setDark(false);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0, top: 10.0),
                  child: Text(
                    "Off",
                    style: ksettingheaderStyle,
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
              ),
              InkWell(
                onTap: () {
                  Provider.of<store>(context, listen: false).setDark(true);
                },
                child: Padding(
                  padding: const EdgeInsets.only(left: 12.0, top: 10.0),
                  child: Text(
                    "Auto",
                    style: ksettingheaderStyle,
                  ),
                ),
              ),
              Divider(
                color: Colors.black,
              )
            ],
          )
        ],
      ),
    ));
  }
}
