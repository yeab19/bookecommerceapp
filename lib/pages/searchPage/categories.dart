import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './search.dart';
import '../../state.dart';
import '../../utils/constants.dart';
import '../categories/bookCategory.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => store(),
      child: Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(top: 12.0),
            child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Colors.black,
                          size: 26,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30, top: 15),
                    child: Text(
                      "Categories",
                      style: ktitleTextStyle,
                    ),
                  ),
                  Divider(
                    color: Colors.grey,
                    thickness: 1,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Education");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1532012197267-da84d127e765?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NXx8ZWR1Y2F0aW9ufGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Education",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Health");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1485527172732-c00ba1bf8929?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Health",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Science Fiction");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1529969790834-4e32e2bb2032?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8c2NpJTIwZml8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Sci-Fi",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Crime");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1592772874383-d08932d29db7?ixid=MXwxMjA3fDB8MHxzZWFyY2h8NHx8Y3JpbWV8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Crime",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Mystery");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1606764979711-81266cf301bc?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTV8fG15c3Rlcnl8ZW58MHx8MHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Mystery",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Romance");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1522033467113-b8db23657b96?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8cm9tYW5jZXxlbnwwfHwwfA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Romance",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Divider(color: Colors.black),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return BookCategory(category: "Thriller");
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(right: 19.0, left: 20.0),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.bottomLeft,
                            height: 166,
                            width: 152,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(
                                      'https://images.unsplash.com/photo-1610201945030-d049e825461c?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fHRocmlsbGVyfGVufDB8fDB8&ixlib=rb-1.2.1&auto=format&fit=crop&w=400&q=60'),
                                  fit: BoxFit.fitWidth),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15)),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "Thriller",
                                  style: kCategoryStyle,
                                  textAlign: TextAlign.end,
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
