import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// import "package:quiver/";
// import 'package:search_page/search_page.dart';

import '../bookDescription/bookDescription.dart';
import '../../utils/constants.dart';
import '../../utils/requests.dart';

List books = [];
var results;
var display = '';
var foundBooks = [];

class SearchBookPage extends StatefulWidget {
  @override
  _SearchBookPageState createState() => _SearchBookPageState();
}

class _SearchBookPageState extends State<SearchBookPage> {
  @override
  var search = TextEditingController();
  static final GlobalKey<FormFieldState<String>> _searchFormKey =
      GlobalKey<FormFieldState<String>>();

  void initState() {
    getBooks();
    search.addListener(_results);
  }

  // void dispose() {
  //   search.dispose();
  //   super.dispose();
  // }

  _results() {
    setState(() {
      print(display);
      display = search.text;
      foundBooks = [];
      foundBooks.clear();

      for (int i = 0; i < books.length; i++) {
        print(books[i]['title'] + " " + search.text);
        // if (books[i]['title']?.toLowerCase() == search.text?.toLowerCase()) {
        //   foundBooks.add(books[i]);
        //   print("Book is found");
        // }
        if (books[i]['title'].contains(search.text) ||
            books[i]['title'].contains(search.text.toLowerCase()) ||
            books[i]['title'].contains(search.text.toUpperCase())) {
          foundBooks.add(books[i]);
          print("Book is found");
        }
      }

      var filteredBooks = checkFilters(foundBooks);

      results = ListView.builder(
          padding: EdgeInsets.all(8.0),
          itemCount: filteredBooks.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: InkWell(
                  child: ListTile(
                    title: Text(filteredBooks[index]['title']),
                  ),
                  onTap: () {
                    // BookDescription
                    print(filteredBooks[index]['title']);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => BookDescription(),
                            settings:
                                RouteSettings(arguments: foundBooks[index])));
                  }),
            );
          });
    });
    print("Second text field: ${search.text}");
  }

  checkFilters(List Books) {
    if (sortBy == "Published Date") {
      // Comparator<UserModel> sortById = (a, b) => a.id.compareTo(b.id);

      Books.sort((a, b) {
        var adate = a['timestamp'];
        var bdate = b['timestamp'];
        return -adate.compareTo(bdate);
        // return Books;
      });
      // print(Books);
    }
    if (price == "Free") {
      List freeBooks = [];
      for (var book in Books) {
        print(book);
        if (book['free'] == true) {
          freeBooks.add(book);
        }
      }
      Books = [];
      Books = freeBooks;
    }
    if (price == "Low to High") {
      Books.sort((a, b) {
        var aprice = a['price'];
        var bprice = b['price'];
        return aprice.compareTo(bprice);
        // return Books;
      });
    }
    if (price == "High to Low") {
      Books.sort((a, b) {
        var aprice = a['price'];
        var bprice = b['price'];
        return -aprice.compareTo(bprice);
        // return Books;
      });
    }
    if (category.isNotEmpty) {
      List categorizedBooks = [];
      for (var book in Books) {
        if (category == book['category']) {
          categorizedBooks.add(book);
        }
      }
      Books = [];
      Books = categorizedBooks;
    }
    return Books;
  }

  filterByTimestamp(List Books) {}
  void getBooks() {
    var response = getallBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        books = data['data'];
        print(books.length);
      } else {
        books = [];
      }
    });
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    print(width / 1.28);
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(
                  Icons.arrow_back,
                  color: buttonColor,
                  size: 30,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              Row(
                children: [
                  Container(
                    width: width / 1.0275,
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: width / 1.28,
                          child: TextField(
                            // key: _searchFormKey,
                            // autofocus: true,
                            controller: search,
                            obscureText: false,
                            style: KTextFieldStyle,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                labelText: "Enter title or author",
                                prefixIcon: Icon(Icons.search),
                                suffixIcon: IconButton(
                                    onPressed: () {
                                      initState();
                                      Navigator.push(context,
                                          MaterialPageRoute(builder: (context) {
                                        return FilterPage();
                                      }));
                                    },
                                    icon: Icon(Icons.sort)),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(32.0))),
                          ),
                        ),
                        Text("Cancel"),
                      ],
                    ),
                  )
                ],
              ),
              Container(
                height: 400,
                child: results,
              )
            ],
          )),
        ),
      ),
    );
  }
}

String sortBy = '';
String price = '';
String category = '';
String reaction = '';

class FilterPage extends StatefulWidget {
  @override
  _FilterPageState createState() => _FilterPageState();
}

class _FilterPageState extends State<FilterPage> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Container(
            padding: EdgeInsets.only(top: 45.0, left: 20.0),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 18.0, top: 10.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Filter", style: kFilterStyle),
                        InkWell(
                          onTap: () {
                            sortBy = '';
                            price = '';
                            category = '';
                            reaction = '';
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(right: 28.0),
                            child: Text(
                              "Cancel",
                              style: kFilterCancelStyle,
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 2,
                          child: DropdownButton<String>(
                              hint: Text(sortBy.isEmpty ? "Sort By" : sortBy),
                              items: <String>[
                                'Popular',
                                'Follower',
                                'Featured',
                                'Published Date'
                              ].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  sortBy = value;
                                });
                              }),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 2,
                          child: DropdownButton<String>(
                              hint: Text(price.isEmpty ? "Price" : price),
                              items: <String>[
                                'Free',
                                'Low to High',
                                'High to Low'
                              ].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  price = value;
                                });
                              }),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 2,
                          child: DropdownButton<String>(
                              hint: Text(
                                  category.isEmpty ? "Category" : category),
                              items: <String>[
                                'Education',
                                'Crime',
                                'Adventure',
                                'Romance',
                                'Sci-Fi',
                                'Thriller'
                              ].map((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  category = value;
                                });
                              }),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Container(
                          width: width / 2,
                          child: DropdownButton<IconData>(
                              hint: Text(
                                  reaction.isEmpty ? "Reactions" : reaction),
                              items: <IconData>[
                                Icons.thumb_up_sharp,
                                CupertinoIcons.heart_solid,
                                Icons.emoji_emotions_rounded,
                                Icons.emoji_emotions_sharp
                              ].map((IconData value) {
                                return DropdownMenuItem<IconData>(
                                  value: value,
                                  child: Icon(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  if (value == Icons.thumb_up_sharp) {
                                    reaction = 'like';
                                  } else if (value ==
                                      CupertinoIcons.heart_solid) {
                                    reaction = 'love';
                                  } else if (value ==
                                      Icons.emoji_emotions_rounded) {
                                    reaction = 'great';
                                  } else {
                                    reaction = 'wow';
                                  }
                                });
                              }),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: height / 2.5),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            MaterialButton(
                                color: buttonColor,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: Text("Apply"))
                          ]),
                    )
                  ],
                ),
              ),
            )));
  }
}
