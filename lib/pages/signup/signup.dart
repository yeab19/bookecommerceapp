import 'package:bookecommerceapp/pages/signin/signin.dart';
import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';
import '../../utils/constants.dart';

final email = TextEditingController();
final password = TextEditingController();
final firstname = TextEditingController();
final lastname = TextEditingController();
final confirmpassword = TextEditingController();

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  void Signup(BuildContext context) {
    print(firstname.text);
    if (email.text != '' &&
        password.text != '' &&
        firstname.text != '' &&
        lastname.text != '' &&
        confirmpassword.text != '') {
      if (password.text == confirmpassword.text) {
        Map Data = {};
        Data['country'] = 'USA';
        // Data['isAdmin'] = false;
        Data['firstname'] = firstname.text;
        Data['lastname'] = lastname.text;
        Data['email'] = email.text;
        Data['password'] = password.text;
        // Data['isAdmin'] = false;

        var response = Register(Data);
        response.then((data) {
          print(data);
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SignIn();
          }));
        });
      }

      print(email.text);
    } else {
      print("Text values not input");
    }
  }

  final firstNameField = TextField(
    controller: firstname,
    obscureText: false,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Firstname",
        prefixIcon: Icon(Icons.account_box),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  final lastNameField = TextField(
    controller: lastname,
    obscureText: false,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Lastname",
        prefixIcon: Icon(Icons.account_box),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  final emailField = TextField(
    controller: email,
    obscureText: false,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Email",
        prefixIcon: Icon(Icons.email),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  final passwordField = TextField(
    controller: password,
    obscureText: true,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Password",
        prefixIcon: Icon(Icons.lock),
        suffixIcon: InkWell(child: Icon(Icons.remove_red_eye)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  final confirmPasswordField = TextField(
    controller: confirmpassword,
    obscureText: true,
    style: KTextFieldStyle,
    decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        labelText: "Confirm Password",
        prefixIcon: Icon(Icons.lock),
        suffixIcon: InkWell(child: Icon(Icons.remove_red_eye)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
  );

  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(25.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              children: [
                Container(
                  // padding: EdgeInsets.only(left: 12.0,right: 20.0),
                  child: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: buttonColor,
                        size: 50.0,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                )
              ],
            ),
            Container(
                padding: EdgeInsets.all(10.0),
                child: Image(
                  image: AssetImage('images/magyz.png'),
                  width: 100,
                  height: 100,
                )),
            Container(
                padding: EdgeInsets.all(10.0),
                child: Text(
                  "Create an Account",
                  style: KSubTitleStyle,
                )),
            Container(
              padding: EdgeInsets.all(10.0),
              child: firstNameField,
            ),
            Container(padding: EdgeInsets.all(10.0), child: lastNameField),
            Container(
              padding: EdgeInsets.all(10.0),
              child: emailField,
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: passwordField,
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: confirmPasswordField,
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: FractionallySizedBox(
                  widthFactor: 1,
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: color,
                    child: MaterialButton(
                      // minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        Signup(context);
                      },
                      // shape: ShapeBorder.,
                      color: buttonColor,
                      // shape: ,
                      child: Text(
                        "Register",
                        style: KTitleStyle,
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
    ));
  }
}
