import 'package:bookecommerceapp/utils/requests.dart';
import 'package:flutter/material.dart';

import '../bookDescription/bookDescription.dart';
import '../bookDescription/coverImage.dart';
import '../bookDescription/bookAuthor.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';

List<bool> isSelected = [false, false];
List FreeBooks = [];
List PaidBooks = [];

class PopularBooks extends StatefulWidget {
  @override
  _PopularBooksState createState() => _PopularBooksState();
}

class _PopularBooksState extends State<PopularBooks> {
  @override
  void initState() {
    getfreeBooks();
    getpaidBooks();
  }

  getfreeBooks() {
    var response = getFreeBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        setState(() {
          FreeBooks = data['data'];
        });
      }
    });
  }

  getpaidBooks() {
    var response = getPaidBooks();
    response.then((data) {
      if (data['status'] == 'success') {
        setState(() {
          PaidBooks = data['data'];
        });
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 30.0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 12.0, right: 20.0),
                  child: IconButton(
                      icon: Icon(
                        Icons.arrow_back,
                        color: buttonColor,
                        size: 40.0,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      }),
                ),
                Text(
                  "Popular Books",
                  style: ktitleTextStyle,
                )
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(left: 25.0, top: 10.0),
              child: Row(
                children: [
                  ToggleButtons(
                      renderBorder: false,
                      children: [
                        Container(
                          width: 80,
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Paid",
                                style: kBookTitleStyle,
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: 80,
                          child: Row(
                            children: <Widget>[
                              Text(
                                "Free",
                                style: kBookTitleStyle,
                              ),
                            ],
                          ),
                        )
                      ],
                      onPressed: (int index) {
                        setState(() {
                          for (int buttonIndex = 0;
                              buttonIndex < isSelected.length;
                              buttonIndex++) {
                            if (buttonIndex == index) {
                              isSelected[buttonIndex] =
                                  !isSelected[buttonIndex];
                            } else {
                              isSelected[buttonIndex] = false;
                            }
                          }
                        });
                      },
                      isSelected: isSelected),
                  Divider(
                    color: Colors.black,
                  ),
                ],
              ),
            ),
            Container(
              child: Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount:
                        isSelected[0] ? PaidBooks.length : FreeBooks.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BookDescription(),
                                      settings: RouteSettings(
                                          arguments: isSelected[0]
                                              ? PaidBooks[index]
                                              : FreeBooks[index])));
                            },
                            child: Container(
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  CoverImage(
                                    book: isSelected[0]
                                        ? PaidBooks[index]
                                        : FreeBooks[index],
                                    height: 208,
                                    width: 137,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    textBaseline: TextBaseline.alphabetic,
                                    children: [
                                      Container(
                                        width: 200,
                                        // BookAuthor(book: books[index])
                                        child: BookAuthor(
                                            book: isSelected[0]
                                                ? PaidBooks[index]
                                                : FreeBooks[index]),
                                      ),
                                      Container(
                                          width: 200,
                                          child: Text(
                                            isSelected[0]
                                                ? PaidBooks[index]['title']
                                                : FreeBooks[index]['title'],
                                            style: kBookTitleHeaderStyle,
                                          )),
                                      !isSelected[0]
                                          ? FlatButton(
                                              onPressed: () {},
                                              child: Text("Free"),
                                              color: buttonColor,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  side: BorderSide(
                                                      color: buttonColor)),
                                            )
                                          : FlatButton(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  side: BorderSide(
                                                      color: buttonColor)),
                                              onPressed: () {},
                                              child: Text(
                                                  PaidBooks[index]['price']),
                                              color: buttonColor,
                                            )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          Divider(
                            color: Colors.black,
                          )
                        ],
                      );
                    }),
              ),
            )
          ],
        ),
      ),
    );
  }
}
