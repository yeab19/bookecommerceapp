import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

import '../../utils/constants.dart';
import '../../utils/requests.dart';
import '../../state.dart';

var messages = [];

class ChatPage extends StatefulWidget {
  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> with WidgetsBindingObserver {
  @override
  Timer timer;
  final message = TextEditingController();
  bool isFocused = false;
  bool isLoading = true;
  bool waitingForResponse = false;
  ScrollController controller;

  // Timer timer;

  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    setTimer(false);
    // message.addListener(_atFocus);
  }

  void dispose() {
    timer?.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    setTimer(state != AppLifecycleState.resumed);
  }

  void setTimer(bool isBackground) {
    print(isBackground);
    if (!isBackground) {
      int delaySeconds = 2;

      timer?.cancel();
      timer = Timer.periodic(Duration(seconds: delaySeconds), (t) async {
        if (!waitingForResponse) {
          waitingForResponse = true;
          await loadMessages();
          waitingForResponse = false;
        }
      });
    }
  }

  _atFocus() {
    // message.addListener(_atFocus);
    if (!isFocused) {
      // setState(() {
      //   isFocused = true;
      // });
    }

    // message.dispose();
  }

  void didChangeDependencies() async {
    // timer = Timer.periodic(
    //     Duration(seconds: 1), (Timer t) => checkForNewSharedLists());
    await loadMessages();
    setState(() {
      isLoading = false;
    });
  }

  Future<void> loadMessages() async {
    final Map user = ModalRoute.of(context).settings.arguments;
    final userid = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    var response = await getConversation(user['id'], userid, token);
    // print(response);
    if (response['data'].length != 0) {
      if (response['data'].length > messages.length) {
        print("New Messages");
      }
      setState(() {
        messages = response['data'];
      });
    }

    // print(userid);
  }

  sendMessages(messageText) async {
    final Map user = ModalRoute.of(context).settings.arguments;
    final userid = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    var Message = {
      "messageText": messageText,
      "senderId": userid,
      "receiverId": user['id']
    };

    setState(() {
      message.text = '';
      messages.add(Message);
      controller.jumpTo(controller.position.maxScrollExtent);
    });

    var response = await sendMessage(Message, token);
    if (response['status'] == "success") {}
  }

  Widget build(BuildContext context) {
    final Map user = ModalRoute.of(context).settings.arguments;
    final userid = Provider.of<store>(context, listen: false).getUserId;

    print(userid);
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    var firstname =
        user['firstname'][0].toUpperCase() + user['firstname'].substring(1);
    var lastname =
        user['lastname'][0].toUpperCase() + user['lastname'].substring(1);
    String fullname = firstname + " " + lastname;

    return Scaffold(
        body: SingleChildScrollView(
      child: !isLoading
          ? Container(
              padding: EdgeInsets.only(top: height / 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          IconButton(
                              icon: Icon(
                                Icons.arrow_back,
                                color: buttonColor,
                                size: 40.0,
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              }),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(fullname, style: kchatNameStyle),
                          )
                        ],
                      ),
                      Icon(Icons.info, size: 30.0, color: buttonColor)
                    ],
                  ),
                  Divider(color: Colors.black),
                  SingleChildScrollView(
                    child: Container(
                      height: height / 1.5,
                      child: messages.length == 0
                          ? Column(
                              children: [Text("No messages here")],
                            )
                          : ListView.builder(
                              controller: controller,
                              itemCount: messages.length,
                              shrinkWrap: true,
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              // physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: EdgeInsets.only(
                                      left: 16, right: 16, top: 10, bottom: 10),
                                  child: Align(
                                      alignment:
                                          userid == messages[index]['senderId']
                                              ? Alignment.topRight
                                              : Alignment.topLeft,
                                      child: Container(
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              color: userid ==
                                                      messages[index]
                                                          ['senderId']
                                                  ? buttonColor
                                                  : Colors.lime),
                                          padding: EdgeInsets.all(16),
                                          child: Text(
                                              messages[index]['messageText'],
                                              style: TextStyle(fontSize: 15)))),
                                );
                              }),
                    ),
                  ),
                  Row(children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12.0, vertical: 2.0),
                      child: Container(
                        width: width / 1.2,
                        color: Colors.white,
                        child: TextField(
                            onTap: () {
                              setState(() {
                                isFocused = true;
                              });
                            },
                            controller: message,
                            style: KTextFieldStyle,
                            decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                labelText: "Type a message",
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12.0)),
                                prefixIcon: Icon(Icons.emoji_emotions_sharp),
                                suffixIcon: !isFocused
                                    ? Icon(Icons.attach_file)
                                    : IconButton(
                                        onPressed: () {
                                          sendMessages(message.text);
                                        },
                                        icon: Icon(Icons.send)))),
                      ),
                    ),
                    Icon(
                      Icons.keyboard_voice_rounded,
                      size: 35,
                    )
                  ]),
                ],
              ),
            )
          : Container(
              height: height,
              child: Center(
                child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 100.0,
                  color: Colors.pink,
                ),
              ),
            ),
    ));
  }
}
