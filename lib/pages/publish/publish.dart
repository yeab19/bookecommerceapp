import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';

import 'publishDetails.dart';
import '../../utils/constants.dart';
import '../../utils/backButton.dart' as helper;
import 'package:flutter/services.dart';

// FilePickerResult result = await FilePicker.platform.pickFiles();
var title = TextEditingController();
var description = TextEditingController();

class PublishPage extends StatefulWidget {
  @override
  _PublishPageState createState() => _PublishPageState();
}

class _PublishPageState extends State<PublishPage> {
  @override
  String fileName;
  String path;
  Map<String, String> paths;
  List<String> extensions;
  bool isLoadingPath = false;
  bool isMultiPick = false;
  FileType fileType = FileType.image;
  FilePickerResult result;
  PlatformFile image;

  void _openFileExplorer() async {
    setState(() => isLoadingPath = true);
    try {
      result = await FilePicker.platform
          .pickFiles(type: fileType, allowedExtensions: extensions);

      image = result.files.first;

      print(image.name);

      paths = null;
      // }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoadingPath = false;
      fileName = path != null
          ? path.split('/').last
          : paths != null
              ? paths.keys.toString()
              : '...';
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 35.0, left: 12.0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  child: helper.BackButton(),
                ),
                Text(
                  "Publish a book",
                  style: ktitleTextStyle,
                )
              ],
            ),
            Divider(color: Colors.black),
            InkWell(
              onTap: () {
                _openFileExplorer();
              },
              child: Container(
                  padding: EdgeInsets.only(left: 73.0),
                  decoration:
                      BoxDecoration(border: Border.all(color: Colors.black12)),
                  child: Column(children: [
                    Row(
                      children: [
                        image != null
                            ? Offstage()
                            : Icon(
                                Icons.photo,
                                color: Colors.white,
                                size: 100.0,
                              ),
                      ],
                    ),
                    Row(
                      children: [
                        Text("Add a cover Picture", style: ksettingStyle),
                      ],
                    ),
                    image != null ? Text(image.path) : Offstage()
                  ])),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 12.0, top: 12.0),
              child: Row(
                children: [
                  Text(
                    "Upload Detail",
                    style: ksettingStyle,
                  )
                ],
              ),
            ),
            Divider(
              color: Colors.black,
            ),
            Row(
              children: [
                Text("Title"),
              ],
            ),
            Theme(
              data: ThemeData(
                primaryColor: buttonColor,
                inputDecorationTheme: InputDecorationTheme(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.amber)),
                ),
              ),
              child: TextField(
                controller: title,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        // borderSide: BorderSide(color:buttonColor),
                        borderRadius: BorderRadius.circular(12.0))),
              ),
            ),

            Row(
              children: [
                Text("Description"),
              ],
            ),
            Theme(
              data: ThemeData(
                primaryColor: buttonColor,
                inputDecorationTheme: InputDecorationTheme(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.amber)),
                ),
              ),
              child: TextField(
                controller: description,
                maxLines: 9,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: buttonColor),
                        borderRadius: BorderRadius.circular(12.0))),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  child: Container(
                    padding: EdgeInsets.only(right: 10.0),
                    child: MaterialButton(
                      color: buttonColor,
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return PublishDetails(
                              title: title.text,
                              description: description.text,
                              coverImage: image.path);
                        }));
                      },
                      child: Text("Next"),
                    ),
                  ),
                )
              ],
            )
            // FilePicker()
          ],
        ),
      ),
    ));
  }
}
