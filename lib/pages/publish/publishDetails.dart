import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:file_picker/file_picker.dart';

import '../../state.dart';
import '../../utils/requests.dart';
import '../../utils/constants.dart';
import '../../utils/backButton.dart' as helper;
import 'package:flutter/services.dart';

class PublishDetails extends StatefulWidget {
  PublishDetails(
      {Key key,
      @required this.title,
      @required this.description,
      @required this.coverImage});

  final String title;
  final String description;
  final String coverImage;

  @override
  _PublishDetailsState createState() => _PublishDetailsState();
}

class _PublishDetailsState extends State<PublishDetails> {
  @override
  String fileName;
  String path;
  Map<String, String> paths;
  List<String> extensions = ['pdf'];
  bool isLoadingPath = false;
  bool isMultiPick = false;
  FileType fileType = FileType.custom;
  FilePickerResult result;
  PlatformFile book;

  List<String> Tags = [
    'Science Fiction',
    'Romance',
    'Drama',
    'Horror',
    'Women',
    'Thriller',
    'Mystery',
    'Fantasy',
    'Health'
  ];
  List<String> Languages = [
    'English',
    'French',
    'Spanish',
    'German',
    'Swahili'
  ];
  List<String> PaymentOptions = ['Free', 'Paid'];
  String _tagdropdown = 'Drama';
  String genredropdown = 'Horror';
  String Languagedropdown = 'English';
  String pricedropdown = 'Paid';
  var price = TextEditingController();

  void _openFileExplorer() async {
    setState(() => isLoadingPath = true);
    try {
      result = await FilePicker.platform
          .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);

      book = result.files.first;

      print(book.name);

      paths = null;
      // }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoadingPath = false;
      fileName = path != null
          ? path.split('/').last
          : paths != null
              ? paths.keys.toString()
              : '...';
    });
  }

  void uploadcoverImage(String bookid) async {
    var token = Provider.of<store>(context, listen: false).getToken;
    print(bookid);
    print(widget.coverImage);
    // setState(() {
    //   isLoadingPath = true;
    // });
    var response = await uploadBookCover(bookid, widget.coverImage, token);
  }

  void uploadbook(String bookid) async {
    var token = Provider.of<store>(context, listen: false).getToken;
    var response = await uploadBook(bookid, book.path, token);
    Navigator.pop(context);
  }

  publishBook() async {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    String token = Provider.of<store>(context, listen: false).getToken;

    String title = widget.title;
    String description = widget.description;

    List<String> tags = [];
    tags.add(_tagdropdown);
    String category = genredropdown;
    String language = Languagedropdown;
    String Price;
    if (pricedropdown == 'Paid') {
      Price = price.text;
    }

    Map data = {
      "title": title,
      "description": description,
      "tags": tags,
      "category": category,
      "free": pricedropdown == 'Paid' ? false : true,
      "price": pricedropdown == 'Paid' ? Price : '0',
      "language": language,
      "createdBy": UserId
    };

    print(tags.runtimeType);
    var response = await addBook(data, token);
    if (response['status'] == "success")
      await uploadcoverImage(response['data']['id']);
    await uploadbook(response['data']['id']);
  }
  // print(response);

  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        padding: EdgeInsets.only(top: 35.0, left: 12.0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  child: helper.BackButton(),
                ),
                Text(
                  "Publish ",
                  style: ktitleTextStyle,
                )
              ],
            ),
            Row(
              children: [
                Container(
                    padding: EdgeInsets.only(
                        left: publishDetailsPadding(context), top: 20),
                    child: Text(
                      "Upload detail",
                      style: ksettingheaderStyle,
                    )),
              ],
            ),
            Divider(
              color: Colors.black,
            ),
            Container(
              padding: EdgeInsets.only(left: publishDetailsPadding(context)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Text(
                  //     "Main Characters",
                  //   style: ksettingheaderStyle,),
                  // Padding(
                  //   padding: const EdgeInsets.symmetric(vertical:5.0),
                  //   child: Container(
                  //
                  //       decoration:BoxDecoration(
                  //           borderRadius:BorderRadius.circular(5),
                  //           border: Border.all(color: buttonColor)
                  //         // color: buttonColor
                  //       ),
                  //       child: TextField()
                  //   ),
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Text("Tags", style: ksettingheaderStyle),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Container(
                      width: 191,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: buttonColor)
                          // color: buttonColor
                          ),
                      child: MyDropDown(
                        itemsList: Tags,
                        value: _tagdropdown,
                        hintText: Text("Enter the tag"),
                        onChanged: (val) {
                          setState(() {
                            _tagdropdown = val;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Text(
                      "Genre",
                      style: ksettingheaderStyle,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Container(
                      width: 191,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: buttonColor)
                          // color: buttonColor
                          ),
                      child: MyDropDown(
                        itemsList: Tags,
                        value: genredropdown,
                        hintText: Text("Enter the genre"),
                        onChanged: (val) {
                          setState(() {
                            genredropdown = val;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Text(
                      "Language",
                      style: ksettingheaderStyle,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Container(
                      width: 191,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(color: buttonColor)
                          // color: buttonColor
                          ),
                      child: MyDropDown(
                        itemsList: Languages,
                        value: Languagedropdown,
                        hintText: Text("Enter the Language"),
                        onChanged: (val) {
                          setState(() {
                            Languagedropdown = val;
                          });
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: Row(
                      children: [
                        Text(
                          "Upload",
                          style: ksettingheaderStyle,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 38.0),
                          child: InkWell(
                            onTap: () {
                              _openFileExplorer();
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.file_upload,
                                  color:
                                      book != null ? Colors.grey : buttonColor,
                                  size: 30,
                                ),
                                book != null
                                    ? Container(
                                        width: 100,
                                        child: Text(
                                          book.name,
                                          // maxLines: 3,
                                        ))
                                    : Offstage()
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(
                      children: [
                        Text(
                          "Price",
                          style: ksettingheaderStyle,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: Container(
                            width: 191,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: buttonColor)
                                // color: buttonColor
                                ),
                            child: MyDropDown(
                              itemsList: PaymentOptions,
                              value: pricedropdown,
                              hintText: Text("Enter the Language"),
                              onChanged: (val) {
                                setState(() {
                                  pricedropdown = val;
                                });
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  pricedropdown == 'Paid' && pricedropdown != null
                      ? Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                            children: [
                              Text(
                                "Price in USD",
                                style: ksettingheaderStyle,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  width: 100,
                                  child: TextField(
                                      controller: price,
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                              // borderSide: BorderSide(color:buttonColor),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      12.0)))),
                                ),
                              )
                            ],
                          ),
                        )
                      : Offstage(),
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: buttonColor)),
                        child: MaterialButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: kcancelbuttonStyle,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 28.0),
                        child: MaterialButton(
                            onPressed: () {
                              publishBook();
                            },
                            color: buttonColor,
                            child: Text(
                              "Publish",
                              style: kAuthorStyle,
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}

class MyDropDown extends StatelessWidget {
  final value;
  final List<String> itemsList;
  final Color dropdownColor;
  final Widget hintText;
  final Function(dynamic value) onChanged;
  const MyDropDown({
    @required this.value,
    @required this.itemsList,
    this.dropdownColor,
    @required this.onChanged,
    @required this.hintText,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print(itemsList.map((String item) => DropdownMenuItem<String>(
    //     value: item,
    //     child: Text(item)
    // )
    // ).toList()[0].toString().characters);
    return Container(
      padding: EdgeInsets.only(left: 20, top: 3, bottom: 3),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          isExpanded: true,
          value: value,
          hint: hintText,
          items: itemsList
              .map((String item) => DropdownMenuItem<String>(
                  value: item,
                  child: Text(
                    item,
                    style: ksettingheaderStyle,
                  )))
              .toList(),
          onChanged: (value) => onChanged(value),
        ),
      ),
    );
  }
}
