import 'package:flutter/material.dart';

class store extends ChangeNotifier {
  var _UserId = '';
  var _Token = '';
  var _isLogged = false;
  var _isDarkTheme = false;
  var _firstname = '';
  var _lastname = '';

  String get getUserId {
    return _UserId;
  }

  String get getfirstname {
    return _firstname;
  }

  String get getlastname {
    return _lastname;
  }

  String get getToken {
    return _Token;
  }

  bool get getisLogged {
    return _isLogged;
  }

  bool get getisDarkTheme {
    return _isDarkTheme;
  }

  void setUserId(userid) {
    _UserId = userid;
    print(_UserId);
    notifyListeners();
  }

  void setFirstname(firstname) {
    _firstname = firstname;
    notifyListeners();
  }

  void setLastname(lastname) {
    _lastname = lastname;
    notifyListeners();
  }

  void setToken(token) {
    _Token = token;
    print(_Token);
    notifyListeners();
  }

  void setLogged() {
    _isLogged = !_isLogged;
    print(_isLogged);
    notifyListeners();
  }

  void setDark(bool value) {
    _isDarkTheme = value;
    notifyListeners();
  }
}
