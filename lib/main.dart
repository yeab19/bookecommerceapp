import 'dart:convert';
import 'package:firebase_core/firebase_core.dart';

import 'package:bookecommerceapp/utils/constants.dart' as prefix0;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import './utils/constants.dart';
import 'package:provider/provider.dart';
import 'state.dart';

// import 'package:firebase_core/firebase_core.dart' as firebase_core;
// import 'package:firebase_storage/firebase_storage.dart' as firebase_storage
import './pages/signin/signin.dart';

// void main() async{
//   WidgetsFlutterBinding.ensureInitialized();
//   await firebase_core.Firebase.initializeApp();
//   runApp(MyApp());
// }

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          print(snapshot);
          if (snapshot.hasError) {
            return MaterialApp(
                home: Scaffold(
              body: Center(
                child: Text("Can't seem to connect with firebase"),
              ),
            ));
          } else {
            return ChangeNotifierProvider<store>(
              create: (_) => store(),
              child: Builder(builder: (BuildContext context) {
                bool isDarkTheme = Provider.of<store>(context).getisDarkTheme;
                return MaterialApp(
                    title: 'Magyz',
                    debugShowCheckedModeBanner: false,
                    darkTheme: ThemeData.dark(),
                    themeMode: isDarkTheme ? ThemeMode.dark : ThemeMode.light,
                    theme: ThemeData(
                        appBarTheme: AppBarTheme(color: color),
                        scaffoldBackgroundColor: color,
                        fontFamily: 'OpenSans'),
                    home: SignIn());
              }),
            );
          }
        });
  }
}

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }
//
// class _MyAppState extends State<MyApp> {
//   bool _initialized = false;
//   bool _error = false;
//
//   // Define an async function to initialize FlutterFire
//   void initializeFlutterFire() async {
//     try {
//       // Wait for Firebase to initialize and set `_initialized` state to true
//       await Firebase.initializeApp();
//       setState(() {
//         _initialized = true;
//       });
//     } catch (e) {
//       // Set `_error` state to true if Firebase initialization fails
//       setState(() {
//         _error = true;
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     initializeFlutterFire();
//     super.initState();
//   }
//
//   Widget build(BuildContext context) {
//     // print(Provider.of<store>(context).getisDarkTheme);
//     if (_initialized) {
//       return ChangeNotifierProvider<store>(
//         create: (_) => store(),
//         child: Builder(builder: (BuildContext context) {
//           bool isDarkTheme = Provider.of<store>(context).getisDarkTheme;
//           return MaterialApp(
//               title: 'Magyz',
//               debugShowCheckedModeBanner: false,
//               darkTheme: ThemeData.dark(),
//               themeMode: isDarkTheme ? ThemeMode.dark : ThemeMode.light,
//               theme: ThemeData(
//                   appBarTheme: AppBarTheme(color: color),
//                   scaffoldBackgroundColor: color,
//                   fontFamily: 'OpenSans'),
//               home: SignIn());
//         }),
//       );
//     } else {
//       return MaterialApp(
//           home: Scaffold(
//             body: Center(
//               child: Text("Can't seem to connect with firebase"),
//             ),
//           ));
//     }
//   }
// }
