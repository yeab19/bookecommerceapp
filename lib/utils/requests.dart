import 'dart:convert';

import 'package:bookecommerceapp/pages/bookDescription/bookDescription.dart';
import 'package:http/http.dart' as http;

final String url = 'https://bookeco.herokuapp.com/api';
// send request to backend login method
Future<Map> login(Map data) async {
  print(data);

  http.Response response = await http
      .post('https://bookeco.herokuapp.com/api/system-users/login', body: data);
  Map Data = jsonDecode(response.body.toString());
  if (Data['error'] != null) {
    if (Data['error']['statusCode'] == 400) {
      var response = {"status": "error"};
      return response;
    }
  } else {
    var response = {"status": "success", "data": Data};

    return response;
  }
}

Future<Map> Register(Map data) async {
  print(data['isAdmin'].runtimeType);
  http.Response response =
      await http.post('$url/system-users/signup', body: data);
  Map Data = {};
  Data = jsonDecode(response.body.toString());

  print(response.body.toString() + "Hello ");
  // Data = jsonDecode(response.body.toString());

  if (Data != null) {
    return {"status": "success", "data": Data};
  } else {
    return {"status": "error", "data": []};
  }
}

//get 10 most recent books
Future<Map> getBooks() async {
  http.Response response = await http.get(
      'https://bookeco.herokuapp.com/api/books?filter={"order": "timestamp DESC","limit":10}');
  List data = [];

  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

//get a specific book by id

Future<Map> getBookById(String id, String token) async {
  http.Response response = await http
      .get('https://bookeco.herokuapp.com/api/books/$id?access_token=$token');

  Map Data = jsonDecode(response.body.toString());
  print("This is the get book by id from all reactions page");

  print(Data);
  if (Data.isNotEmpty) {
    return {"status": "success", "data": Data};
  } else {
    return {"status": "error", "data": []};
  }
}

//get 10 free books
Future<Map> getFreeBooks() async {
  print("Here to get free Books.");
  http.Response response = await http.get(
      'https://bookeco.herokuapp.com/api/books?filter[where][free]=true&&[limit]=10');

  List data = [];

  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

// get 10 Paid Books
Future<Map> getPaidBooks() async {
  print("Here to get Paid Books.");
  http.Response response = await http.get(
      'https://bookeco.herokuapp.com/api/books?filter[where][free]=false&&[limit]=10');
  List data = [];
  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

// get all books
Future<Map> getallBooks() async {
  http.Response response =
      await http.get('https://bookeco.herokuapp.com/api/books');

  List data = [];
  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

//react to a Book
React(Map data, String token) async {
  http.Response response = await http.post(
      'https://bookeco.herokuapp.com/api/books/Rate?access_token=$token',
      body: data);
  Map Data = jsonDecode(response.body.toString());
  print(Data);
}

// save a book
SaveBook(Map data, String token) async {
  http.Response response = await http.post(
      'https://bookeco.herokuapp.com/api/system-users/saveBook?access_token=$token',
      body: data);
  Map Data = jsonDecode(response.body.toString());
  print(Data);
}

//check if a user has saved a specific Book
Future<bool> checkBookSaved(String BookId, String UserId, String token) async {
  http.Response response = await http.get(
      'https://bookeco.herokuapp.com/api/system-users/$UserId?access_token=$token');
  Map Data = jsonDecode(response.body.toString());
  if (Data['savedBooks'].contains(BookId)) {
    return true;
  } else {
    return false;
  }
  print(Data['savedBooks'].toString());
}

//check if a user has liked a specific book
Future<bool> checkBookLiked(String BookId, String UserId, String token) async {
  print("Check if Book is liked man.");
  http.Response response =
      // await http.get('$url/books?filter[where][LikedBy]?access_token=$token');
      await http.get('$url/books/$BookId?access_token=$token');
  Map Data = jsonDecode(response.body.toString());
  print(Data);

  if (Data['LikedBy'] != null) {
    if (Data['LikedBy'].contains(UserId)) {
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

//check if a user has added a specific book to their cart
// Future<bool>
// get the newest 15 books
Future<Map> getNewBooks() async {
  http.Response response = await http.get(
      'https://bookeco.herokuapp.com/api/books?filter={"order": "timestamp DESC","limit":15}');
  List data = [];

  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

// get all Saved Books by a user
Future<Map> getUsersSavedBooks(String UserId, String token) async {
  var Data = {"userId": UserId};
  http.Response response = await http
      .post('$url/system-users/allSavedBooks?access_token=$token', body: Data);

  Map data = {};
  data = jsonDecode(response.body.toString());
  if (data['books'] != null) {
    return {"status": "success", "data": data['books']};
  } else {
    return {"status": "error", "data": []};
  }
}

// get the cart for a User
Future<Map> getCartforUser(String UserId, String token) async {
  var Data = {"userId": UserId};
  http.Response response = await http
      .post('$url/Carts/getCartforUser?acsess_token=$token', body: Data);

  // print(response.body);
  Map data = {};
  data = jsonDecode(response.body.toString());
  if (data != null && data['cart']['status'] == "Found") {
    return {"status": "success", "data": data['cart']['cart']};
  } else {
    return {"status": "error", "data": []};
  }
}

//create cart for user
Future<Map> createCart(String UserId, String token) async {
  var Data = {"userId": UserId};

  http.Response response = await http
      .post("$url/Carts/addcartForUser?access_token=$token", body: Data);

  List data = [];
  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", data: data};
  } else {
    return {"status": "success", "data": []};
  }
}

// get books in a cart
Future<Map> getBooksinCart(String cartId, String token) async {
  var data = {"CartId": cartId};

  http.Response response = await http
      .post('$url/Carts/getBooksinCart?access_token=$token', body: data);
  Map Data = {};
  Data = jsonDecode(response.body.toString());

  if (Data['Books'] != null) {
    return {"status": "success", "data": Data['Books']};
  } else {
    return {"status": "error", "data": []};
  }
  print(response.body);
}

//add book to cart of user
Future<Map> addBooktoCart(String bookId, String cartId, String token) async {
  var Data = {"cartId": cartId, "bookId": bookId};
  print(Data);

  http.Response response = await http
      .post('$url/Carts/addBookToCart?access_token=$token', body: Data);

  print(response.body);
  Map data = {};
  data = jsonDecode(response.body.toString());
  if (data['cart'] != null) {
    return {"status": "success", data: "added"};
  } else {
    return {"status": "success", "data": []};
  }
}

// get all comments for a book
Future<Map> getCommentsForBook(String BookId, String token) async {
  var Data = {"BookId": BookId};

  http.Response response =
      await http.post('$url/books/comments?access_token=$token', body: Data);
  print(response.body + " " + "Hey");
  print("No response for comments");
  Map data = {};
  data = jsonDecode(response.body.toString());
  if (data != null) {
    return {"status": "success", "data": data['Comments']};
  } else {
    return {"status": "error", "data": []};
  }
}

// get the user's details
Future<Map> getUserDetails(String UserId, String token) async {
  http.Response response =
      await http.get('$url/system-users/$UserId?access_token=$token');

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": {}};
  }
}

//get 10 authors of book
Future<Map> getAuthors(String token) async {
  http.Response response = await http
      .post('$url/system-users/getAuthors?access_token=$token?filter[limit]=5');

  // print(response.body.toString());
  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data["Authors"]};
  } else {
    return {"status": "error", "data": {}};
  }
}

Future<Map> getAllAuthors(String token) async {
  http.Response response =
      await http.post('$url/system-users/getAuthors?access_token=$token');

  // print(response.body.toString());
  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data["Authors"]};
  } else {
    return {"status": "error", "data": {}};
  }
}

Future<Map> commentBook(Map Data, String token) async {
  http.Response response =
      await http.post('$url/books/comment?access_token=$token', body: Data);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", data: data};
  } else {
    return {"status": "error", data: {}};
  }
}

Future<Map> getAuthorBooks(String userid, String token) async {
  Map request = {"userid": userid};
  http.Response response = await http
      .post('$url/system-users/getBooks?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data['Books']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getFollowers(String userid, String token) async {
  Map request = {"userid": userid};
  http.Response response = await http
      .post('$url/system-users/followers?access_token=$userid', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data['Followers']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getFollowing(String userid, String token) async {
  Map request = {"userid": userid};
  http.Response response = await http
      .post('$url/system-users/following?access_token=$userid', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data['Following']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getUserLibrary(String userid, String token) async {
  Map request = {"Userid": userid};
  http.Response response = await http
      .post('$url/system-users/getLibrary?access_token=$userid', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data != null) {
    return {"status": "success", "data": data['Books']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getConversation(String user1, String user2, String token) async {
  Map request = {"UserId1": user1, "UserId2": user2};

  http.Response response = await http
      .post('$url/Messages/conversation?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['Messages'].length != 0) {
    return {"status": "success", "data": data['Messages']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> sendMessage(Map request, String token) async {
  http.Response response =
      await http.post('$url/Messages?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  print(data);
  if (data != null) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getBookByCategory(String category, String token) async {
  http.Response response =
      await http.get('$url/books?filter[where][category]=$category');

  List data = [];
  data = jsonDecode(response.body.toString());

  if (data.length != 0) {
    return {"status": "success", "data": data};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> getBookStatus(String Bookid, String token) async {
  Map request = {"BookId": Bookid};

  http.Response response =
      await http.post('$url/books/status?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data.isNotEmpty) {
    return {"status": "success", "data": data['status']};
  } else {
    return {"status": "error", "data": []};
  }
}

Future<Map> resetPassword(String email) async {
  Map request = {"email": email};
  http.Response response = await http
      .post('$url/system-users/resetForgottenPassword', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['response'] != null && data['response'] == 'success') {
    return {"status": "success"};
  } else {
    return {"status": "error"};
  }
}

Future<Map> changePassword(String password, String token) async {
  Map request = {"newPassword": password};

  http.Response response = await http.post(
      '$url/system-users/reset-password?access_token=$token',
      body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  // if(data[''])
}

Future<Map> editUserDetails(String FirstName, String LastName, String email,
    String id, String token) async {
  Map request = {
    "firstname": FirstName,
    "lastname": LastName,
    "email": email,
  };

  http.Response response = await http
      .patch('$url/system-users/$id?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['error'] == null) {
    return {"status": "success"};
  } else {
    return {"status": "error"};
  }
  print(data);
}

Future<Map> buywithStripe(
    String UserId, String BookId, String CardNumber, String token) async {
  Map request = {"UserId": UserId, "BookId": BookId, "CardNumber": CardNumber};
  print("I am here");
  http.Response response = await http
      .post('$url/Carts/buyWithStripe?access_token=$token', body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['result'] != null) {
    return {"status": "success"};
  } else {
    return {"status": "error"};
  }
  print(response.body.toString());
}

// Future<Map> editUser() {}
Future<Map> editPassword(String password, String token) async {
  Map request = {"newPassword": password};

  http.Response response = await http.post(
      '$url/system-users/reset-password?access_token=$token',
      body: request);

  print(response.body);
  // var data;
  // data = jsonDecode(response.body.toString());

  if (response.body != null) {
    return {"status": "success"};
  } else {
    return {"status": "error"};
  }
}

Future<Map> follow(String userId, String user2Id, String token) async {
  Map request = {"friendId": user2Id};

  http.Response response = await http.post(
      '$url/system-users/$userId/follow-friend?access_token=$token',
      body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['data']['success']) {
    return {
      "status": "success",
    };
  } else {
    return {"status": "error"};
  }
}

Future<Map> unfollow(String userId, String user2Id, String token) async {
  Map request = {"friendId": user2Id};

  http.Response response = await http.post(
      '$url/system-users/$userId/unfollow-friend?access_token=$token',
      body: request);

  Map data = {};
  data = jsonDecode(response.body.toString());

  if (data['data']['success']) {
    return {
      "status": "success",
    };
  } else {
    return {"status": "error"};
  }
}

Future<Map> addBook(Map data, String token) async {
  print(data['tags'].runtimeType);

  http.Response response = await http.post('$url/books?access_token=$token',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      // encoding: ,
      body: json.encode(data));

  print(response.body);
  Map Data = {};
  Data = jsonDecode(response.body.toString());

  if (Data != null) {
    return {"status": "success", "data": Data};
  } else {
    return {"status": "error", "data": {}};
  }
}

Future<Map> uploadUserImage(String userid, String path, String token) async {
  var posturl =
      Uri.parse('$url/bookFiles/$userid/uploadUserImage?access_token=$token');
  http.MultipartRequest request = new http.MultipartRequest("POST", posturl);

  http.MultipartFile multipartFile =
      await http.MultipartFile.fromPath('file', path);

  request.files.add(multipartFile);
  http.StreamedResponse response = await request.send();
  print(response.toString());
  // request.files.add(new http.MultipartFile.fromBytes(
  //     'file',await File.fromUri(path).readAsBytes(),contentType: new MediaType))
}

Future<Map> uploadBook(String userid, String path, String token) async {
  var posturl =
      Uri.parse('$url/bookFiles/$userid/uploadBook?access_token=$token');
  http.MultipartRequest request = new http.MultipartRequest("POST", posturl);

  http.MultipartFile multipartFile =
      await http.MultipartFile.fromPath('file', path);

  request.files.add(multipartFile);
  http.StreamedResponse response = await request.send();
  print(response.toString());
  // request.files.add(new http.MultipartFile.fromBytes(
  //     'file',await File.fromUri(path).readAsBytes(),contentType: new MediaType))
}

Future<Map> uploadBookCover(String bookid, String path, String token) async {
  var posturl = Uri.parse(
      '$url/bookFiles/$bookid/uploadBookCoverImage?access_token=$token');
  http.MultipartRequest request = new http.MultipartRequest("POST", posturl);

  http.MultipartFile multipartFile =
      await http.MultipartFile.fromPath('file', path);

  request.files.add(multipartFile);
  http.StreamedResponse response = await request.send();
  print(response.toString());
  // request.files.add(new http.MultipartFile.fromBytes(
  //     'file',await File.fromUri(path).readAsBytes(),contentType: new MediaType))
}
