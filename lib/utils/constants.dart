import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/services.dart';

const KTextFieldStyle = TextStyle(fontSize: 20.0, color: Colors.grey);

const kuserinfoblackStyle = TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.w300,
);

const KSearchFieldStyle = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w300,
    // height: 24.51,
    color: Colors.white);

const ktextStyle = TextStyle(
    fontSize: 18.0,
    fontWeight: FontWeight.w100,
    color: const Color(0xffA39A9A));

const kaboutStyle = TextStyle(
  fontSize: 18.0,
  fontWeight: FontWeight.w100,
  fontFamily: 'OpenSans',
);

const khintStyle = TextStyle(fontSize: 18.0, color: const Color(0xffA39A9A));

const KSubTitleStyle = TextStyle(
    fontSize: 24.0,
    fontWeight: FontWeight.w300,
    color: const Color(0xffA39A9A));

const kFollowButtonStyle = TextStyle(
    fontSize: 20.0, fontWeight: FontWeight.normal, color: Colors.white);
const KTitleStyle =
    TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: Colors.white);
const kFilterStyle =
    TextStyle(fontSize: 24.0, fontWeight: FontWeight.w700, color: Colors.black);
const kAboutStyle = TextStyle(
    fontSize: 28.0, fontFamily: 'OpenSans', fontWeight: FontWeight.normal);
const kNameStyle = TextStyle(
    fontSize: 48.0,
    fontFamily: 'OpenSans',
    fontWeight: FontWeight.normal,
    color: buttonColor);
const kFilterCancelStyle = TextStyle(
    fontSize: 24.0, fontWeight: FontWeight.w700, color: subheaderColor);
const kprofileStyle =
    TextStyle(fontSize: 24.0, fontWeight: FontWeight.w100, color: Colors.white);

const KHeadlineStyle = TextStyle(
  fontFamily: 'OpenSans',
  fontSize: 18.0,
  fontWeight: FontWeight.w600,
  decoration: TextDecoration.underline,
);
const ktitleTextStyle = TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 28.0,
    fontFamily: 'OpenSans',
    color: Colors.black);

const kprofiletitleStyle = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 28.0,
    fontFamily: 'OpenSans',
    color: Colors.white);

const kprofileusernameStyle2 = TextStyle(
  fontWeight: FontWeight.w100,
  fontSize: 16.0,
  fontFamily: 'OpenSans',
);

const kprofileusernameStyle = TextStyle(
    fontWeight: FontWeight.w100,
    fontSize: 16.0,
    fontFamily: 'OpenSans',
    color: Colors.white);
const KHeaderTextStyle = TextStyle(
    fontWeight: FontWeight.w600, fontSize: 28.0, fontFamily: 'OpenSans');
const signupStyle =
    TextStyle(color: buttonColor, fontWeight: FontWeight.bold, fontSize: 16.0);

const KlinkStyle =
    TextStyle(color: buttonColor, fontSize: 18.0, fontWeight: FontWeight.w300);

const kSubHeaderStyle = TextStyle(
  color: subheaderColor,
  fontSize: 16.0,
);

const kAuthornameStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.normal,
);
const kBookAuthorStyle = TextStyle(color: subheaderColor, fontSize: 18.0);

const kBookTitleStyle = TextStyle(
  fontSize: 18.0,
  fontFamily: 'OpenSans',
  fontWeight: FontWeight.bold,
);

const kBookTitleHeaderStyle = TextStyle(
    fontSize: 20.0, fontFamily: 'OpenSans', fontWeight: FontWeight.bold);

const kLibraryStyle = TextStyle(
    fontWeight: FontWeight.normal, fontSize: 24.0, fontFamily: 'OpenSans');

const kGenreStyle = TextStyle(
    fontWeight: FontWeight.w700,
    fontSize: 24.0,
    fontFamily: 'OpenSans',
    color: buttonColor);

const kAuthorStyle = TextStyle(
    fontFamily: 'OpenSans', fontSize: 20.0, fontWeight: FontWeight.w700);

const kNoofBooksStyle = TextStyle(
    fontWeight: FontWeight.w300,
    fontSize: 18.0,
    fontFamily: 'OpenSans',
    color: subheaderColor);

const kWorkStyle = TextStyle(
  fontWeight: FontWeight.normal,
  fontSize: 18.0,
  fontFamily: 'OpenSans',
);

const kCategoryStyle = TextStyle(
    fontFamily: 'OpenSans',
    color: Colors.black,
    fontSize: 18.0,
    fontWeight: FontWeight.w600);

const ktabBarStyle = TextStyle(
    fontFamily: 'OpenSans',
    color: tabBarColor,
    fontWeight: FontWeight.w300,
    fontSize: 20);

const kJoinDateStyle = TextStyle(
    fontFamily: 'OpenSans',
    color: tabBarColor,
    fontWeight: FontWeight.w500,
    fontSize: 20);

const ktabBarViewStyle = TextStyle(
    fontFamily: 'OpenSans',
    color: tabBarColor,
    fontWeight: FontWeight.w500,
    fontSize: 20);

const ksettingTitleStyle = TextStyle(
    fontFamily: 'OpenSans',
    color: subheaderColor,
    fontWeight: FontWeight.normal,
    fontSize: 20);

const kchatNameStyle = TextStyle(
    fontFamily: 'OpenSans', fontWeight: FontWeight.w400, fontSize: 20);

const kbookDescriptionStyle =
    TextStyle(fontFamily: 'OpenSans', color: Colors.black, fontSize: 16.0);

const kReactionStyle = TextStyle(
    fontWeight: FontWeight.w500, fontSize: 20.0, color: subheaderColor);
const kAddtoCartStyle = TextStyle(
    fontWeight: FontWeight.w500, fontSize: 18.0, fontFamily: 'OpenSans');

const kPriceStyle = TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 18.0,
    color: buttonColor,
    fontFamily: 'OpenSans');

const ksettingStyle = TextStyle(
    fontWeight: FontWeight.normal, fontFamily: 'OpenSans', fontSize: 18.0);

const ksettingheaderStyle = TextStyle(
    fontWeight: FontWeight.w500, fontFamily: 'OpenSans', fontSize: 20.0);

const ksettingtextStyle = TextStyle(
    fontWeight: FontWeight.w500, fontFamily: 'OpenSans', fontSize: 20.0);

const kcancelbuttonStyle = TextStyle(
    fontFamily: 'OpenSans',
    fontSize: 20.0,
    color: buttonColor,
    fontWeight: FontWeight.w700);

const kcommentUsernameStyle =
    TextStyle(fontFamily: 'OpenSans', fontSize: 16.0, color: subheaderColor);

const kcommentBodyStyle = TextStyle(
    fontFamily: 'OpenSans', fontSize: 14.0, fontWeight: FontWeight.w200);

const kreactionStyle = TextStyle(
  fontFamily: 'OpenSans',
  fontSize: 20.0,
);
const kuseragreementStyle = TextStyle(
  fontSize: 14.0,
  fontFamily: 'OpenSans',
);

const knoofbooksStyle = TextStyle(
    fontSize: 18.0,
    color: subheaderColor,
    fontFamily: 'OpenSans',
    fontWeight: FontWeight.w100);

const kLibraryDetailStyle = TextStyle(
    fontSize: 14.0, fontFamily: 'OpenSans', fontWeight: FontWeight.w100);

double leftPadding(BuildContext context) {
  print(MediaQuery.of(context).size.width / 12.465);
  return MediaQuery.of(context).size.width / 12.465;
}

double publishDetailsPadding(BuildContext context) {
  print(MediaQuery.of(context).size.width / 10.545);
  return MediaQuery.of(context).size.width / 10.545;
  return MediaQuery.of(context).size.width / 10.545;
}

double commentTimestampPadding(BuildContext context) {
  return MediaQuery.of(context).size.width / 4.195;
}

const color = const Color(0xffFFF7CD);
const buttonColor = const Color(0xffEFC300);
const subheaderColor = const Color(0xffA39A9A);
const iconColor = const Color(0xffC4C4C4);
const bookDescColor = const Color(0xffB4CDCD);
const tabBarColor = const Color(0xff0F0E0E);
const dropdownColor = const Color(0xff888E91);
const profileColor = const Color(0xff94B9C9);
