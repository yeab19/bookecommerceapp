import 'package:flutter/material.dart';

import 'constants.dart';

class BackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right:20.0),
      child: IconButton(
        onPressed: (){
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.arrow_back,
          color: buttonColor,
          size: 48.0,
        ),
      ),
    );
  }
}

