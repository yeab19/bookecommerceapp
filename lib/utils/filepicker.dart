import 'package:bookecommerceapp/utils/constants.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CoverImagePicker extends StatefulWidget {
  @override
  _CoverImagePickerState createState() => _CoverImagePickerState();
}

class _CoverImagePickerState extends State<CoverImagePicker> {
  String fileName;
  String path;
  Map<String, String> paths;
  List<String> extensions;
  bool isLoadingPath = false;
  bool isMultiPick = false;
  FileType fileType = FileType.image;
  FilePickerResult result;

  void _openFileExplorer() async {
    setState(() => isLoadingPath = true);
    try {
      // if (isMultiPick) {
      //   path = null;
      //   paths = await FilePicker.getMultiFilePath(type: fileType? fileType: FileType.any, allowedExtensions: extensions);
      // }
      // else {
      result = await FilePicker.platform
          .pickFiles(type: fileType, allowedExtensions: extensions);
      print(result);
      paths = null;
      // }
    } on PlatformException catch (e) {
      print("Unsupported operation" + e.toString());
    }
    if (!mounted) return;
    setState(() {
      isLoadingPath = false;
      fileName = path != null
          ? path.split('/').last
          : paths != null
              ? paths.keys.toString()
              : '...';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: InkWell(
          onTap: () {
            _openFileExplorer();
          },
          child: Container(
            padding: EdgeInsets.only(left: 73.0),
            decoration:
                BoxDecoration(border: Border.all(color: Colors.black12)),
            child: Column(children: [
              Row(
                children: [
                  Icon(
                    Icons.photo,
                    color: Colors.white,
                    size: 100.0,
                  ),
                ],
              ),
              Row(
                children: [
                  Text("Add a cover Picture", style: ksettingStyle),
                ],
              ),
              new Builder(
                builder: (BuildContext context) => isLoadingPath
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: const CircularProgressIndicator())
                    : path != null || paths != null
                        ? new Container(
                            padding: const EdgeInsets.only(bottom: 30.0),
                            height: MediaQuery.of(context).size.height * 0.50,
                            child: new Scrollbar(
                                child: new ListView.separated(
                              itemCount: paths != null && paths.isNotEmpty
                                  ? paths.length
                                  : 1,
                              itemBuilder: (BuildContext context, int index) {
                                final bool isMultiPath =
                                    paths != null && paths.isNotEmpty;
                                final int fileNo = index + 1;
                                final String name = 'File $fileNo : ' +
                                    (isMultiPath
                                        ? paths.keys.toList()[index]
                                        : fileName ?? '...');
                                final filePath = isMultiPath
                                    ? paths.values.toList()[index].toString()
                                    : path;
                                return new ListTile(
                                  title: new Text(
                                    name,
                                  ),
                                  subtitle: new Text(filePath),
                                );
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      new Divider(),
                            )),
                          )
                        : new Container(),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
