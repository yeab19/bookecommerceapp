import 'package:bookecommerceapp/utils/constants.dart';
import '../state.dart';
import './requests.dart';

import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';

class CustomDialog extends StatefulWidget {
  CustomDialog({Key key, @required this.bookId});

  final bookId;

  @override
  _CustomDialogState createState() => _CustomDialogState();
}

class _CustomDialogState extends State<CustomDialog> {
  bool success = false;
  String successText = '';

  react(String reactionName) async {
    final UserId = Provider.of<store>(context, listen: false).getUserId;
    final token = Provider.of<store>(context, listen: false).getToken;

    Map requestData = {};
    requestData['ReactionName'] = reactionName;
    requestData['UserId'] = UserId;
    requestData['BookId'] = widget.bookId;

    setState(() {
      success = true;
      successText = "You have successfully reacted " + reactionName;
    });
    Future.delayed(Duration(seconds: 2), () {
      Navigator.pop(context);
    });
    await React(requestData, token);
  }

  @override
  void dispose() {
    success = false;
    successText = '';
    super.dispose();
  }

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Container(
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: EdgeInsets.only(top: height / 5, bottom: height / 3.5),
          child: Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            elevation: 0,
            child: Container(
              // height:height/5,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 38.0, vertical: 10.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  react("love");
                                },
                                child: Container(
                                    child: Image(
                                  image: AssetImage('images/Framelove.png'),
                                )),
                              ),
                              Text("Love", style: kreactionStyle)
                            ],
                          ),
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  react("perfect");
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.red,
                                  ),
                                  child: Image(
                                    image:
                                        AssetImage('images/Groupperfect.png'),
                                  ),
                                ),
                              ),
                              Text("Perfect", style: kreactionStyle)
                            ],
                          ),
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  react("wow");
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color(0xffF9C021),
                                  ),
                                  child: Image(
                                    image: AssetImage('images/Groupwow.png'),
                                  ),
                                ),
                              ),
                              Text("Wow", style: kreactionStyle)
                            ],
                          ),
                        ]),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 38.0, vertical: 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                react("great");
                              },
                              child: Container(
                                child: Image(
                                  image: AssetImage('images/Framegreat.png'),
                                ),
                              ),
                            ),
                            Text("Great", style: kreactionStyle)
                          ],
                        ),
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                react("unhappy");
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xffF9C021),
                                ),
                                child: Image(
                                  image: AssetImage('images/GroupUnhappy.png'),
                                ),
                              ),
                            ),
                            Text("Unhappy", style: kreactionStyle)
                          ],
                        ),
                        Column(
                          children: [
                            InkWell(
                              onTap: () {
                                react("poor");
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Color(0xffF9C021),
                                ),
                                child: Image(
                                  image: AssetImage('images/Framepoor.png'),
                                ),
                              ),
                            ),
                            Text("Poor", style: kreactionStyle)
                          ],
                        )
                      ],
                    ),
                  ),
                  Center(child: Text(successText)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
